<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN AKTE KELAHIRAN BARU</h4>
                <p class="card-description">
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#buatakte">Buat Akte Lahir Baru</button>
                </p>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nama Pelapor</td>
                                <td>Nama Anak</td>
                                <td>Nomor Hp</td>
                                <td>Tanggal Pengajuan</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from akte_baru_lahir where petugas='$username' AND status='pending' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {

                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $d['noreg']; ?></td>
                                    <td><?= $d['nama_ayah'] ?></td>
                                    <td><?= $d['nama_anak'] ?></td>
                                    <td><?= $d['no_hp'] ?></td>
                                    <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                    <td>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#hapus<?= $no ?>">Hapus</button>


                                        <!-- BEGIN  modal detail -->
                                        <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form action="scripts/function_desa.php?act=kkbaru" method="POST" enctype="multipart/form-data">

                                                        <?php
                                                        $id = $d['id'];
                                                        $query2 = "SELECT * FROM akte_baru_lahir WHERE id='$id'";
                                                        $result = mysqli_query($connection, $query2);
                                                        while ($row2 = mysqli_fetch_assoc($result)) {

                                                        ?>


                                                            <div class="modal-header">
                                                                <h2 class="modal-title">Detail Akte Kelahiran <?= $row2['nama_anak'] ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <label class="form-label">Tanggal Pengajuan</label>
                                                                    <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Email Desa</label>
                                                                    <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP Desa</label>
                                                                    <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>DATA PELAPOR</h3>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No Kartu Keluarga</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['no_kk'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">NIK Ayah</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nik_ayah'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Ayah</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_ayah'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">NIK Ibu</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nik_ibu'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Ibu</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_ibu'] ?>" readonly>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>DATA ANAK</h3>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Lengkap Anak</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_anak'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Jenis Kelamin</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['jk_anak'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Tanggal Lahir</label>
                                                                    <input type="date" class="form-control" value="<?= $row2['tgl_lahir'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Jenis Kelahiran</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['jenis_kelahiran'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Anak Ke</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['anak_ke'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Berat Anak</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['berat'] ?> gr" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Panjang Anak</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['panjang'] ?> cm" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Detail Pengajuan Berkas</label>
                                                                    <textarea class="form-control" name="detail" id="" cols="30" rows="10" readonly><?= $row2['detail'] ?></textarea>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>BERKAS PERSYARATAN</h3>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['surat_pengantar']; ?>">Surat Pengantar</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['surat_lahir']; ?>">Surat Kelahiran</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['kk']; ?>">KK Orang Tua</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['ktp_ayah']; ?>">KTP Ayah</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['ktp_ibu']; ?>">KTP Ibu</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['buku_nikah']; ?>">Buku Nikah</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte lahir/<?php echo $row2['lainnya']; ?>">Berkas Lainnya</a>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END  modal detail -->

                                        <!-- modal delete -->
                                        <div class="modal fade" id="hapus<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                            hapus
                                                            pengajuan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 align="center">Menghapus Pengajuan atas nama <?= $d['nama_ayah']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <a href="scripts/function_desa.php?act=deleteakte&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal delete -->




                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<!-- BEGIN  modal buat akte kelahiran -->
<div class="modal fade" id="buatakte" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=aktebaru" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Buat Akte Kelahiran</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No Kartu Keluarga</label>
                        <input type="number" class="form-control" name="no_kk" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Ayah</label>
                        <input type="text" class="form-control" name="nik_ayah" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Ayah</label>
                        <input type="text" class="form-control" name="nama_ayah" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Ibu</label>
                        <input type="text" class="form-control" name="nik_ibu" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Ibu</label>
                        <input type="text" class="form-control" name="nama_ibu" required>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA ANAK</h3>
                    <div class="form-group row">
                        <label class="form-label">Nama Lengkap Anak</label>
                        <input type="text" class="form-control" name="nama_anak" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Jenis Kelamin</label>
                        <select name="jk_anak" class="form-control" required>
                            <option value=""></option>
                            <option value="Laki-laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tgl_lahir" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Jenis Kelahiran</label>
                        <select name="jenis_kelahiran" class="form-control" required>
                            <option value="Tunggal">Tunggal</option>
                            <option value="Kembar Dua">Kembar Dua</option>
                            <option value="Kembar Tiga">Kembar Tiga</option>
                            <option value="Kembar Empat">Kembar Empat</option>
                            <option value="Kembar Banyak Lainnya...">Kembar Banyak Lainnya..</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Anak Ke</label>
                        <input type="number" class="form-control" name="anak_ke" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Berat Anak</label>
                        <input type="text" class="form-control" name="berat" placeholder="Isi berat anak dalam satuan gram">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Panjang Anak</label>
                        <input type="text" class="form-control" name="panjang" placeholder="Isi panjang anak dalam satuan cm">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Detail Pengajuan Berkas</label>
                        <textarea class="form-control" name="detail" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label class="form-label">Surat Pengantar RS/Puskesmas</label>
                        <input type="file" class="form-control" name="surat_pengantar">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Kelahiran / SPTJM Kelahiran</label>
                        <input type="file" class="form-control" name="surat_lahir" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KK Orang Tua</label>
                        <input type="file" class="form-control" name="kk">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KTP Ayah</label>
                        <input type="file" class="form-control" name="ktp_ayah">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KTP Ibu</label>
                        <input type="file" class="form-control" name="ktp_ibu">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Buku Nikah / SPTJM</label>
                        <input type="file" class="form-control" name="buku_nikah">
                        <input type="text" name="petugas" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Berkas Lainnya</label>
                        <input type="file" class="form-control" name="lainnya">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat akte kelahiran -->


<?php
include 'footer.php';
?>