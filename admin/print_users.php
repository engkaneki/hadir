<?php
require('library/fpdf.php');
include 'scripts/koneksi.php';

function tanggal_indonesia($tanggal)
{

    $bulan = array(
        1 =>    'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    $var = explode('-', $tanggal);

    return $var[2] . ' ' . $bulan[(int)$var[1]] . ' ' . $var[0];
}

$pdf = new FPDF('P', 'mm', 'A4');
$pdf->AddPage();

$pdf->SetFont('Times', 'B', 14);
$pdf->Cell(200, 10, 'DAFTAR DESA PENGGUNA LAYANAN', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 1);
$pdf->SetFont('Times', 'B', 14);
$pdf->Cell(200, 10, 'DUKCAPIL HADIR DI DESA', 0, 0, 'C');


$pdf->Cell(5, 15, '', 0, 1);
$pdf->SetFont('Times', 'B', 11);
$pdf->Cell(10, 7, 'NO', 1, 0, 'C');
$pdf->Cell(63, 7, 'DESA', 1, 0, 'C');
$pdf->Cell(58, 7, 'NAMA KADES', 1, 0, 'C');
$pdf->Cell(63, 7, 'NAMA OPERATOR', 1, 0, 'C');


$pdf->Cell(5, 7, '', 0, 1);
$pdf->SetFont('Times', '', 11);
$no = 1;
$data = mysqli_query($connection, "SELECT  * FROM tbl_users WHERE role='desa'");
while ($d = mysqli_fetch_array($data)) {

    $pdf->Cell(10, 10, $no++, 1, 0, 'C');
    $pdf->Cell(63, 10, $d['nama_desa'], 1, 0);
    $pdf->Cell(58, 10, $d['nama_kades'], 1, 0);
    $pdf->Cell(63, 10, $d['nama_pengguna'], 1, 1);
}

$pdf->Output('', 'Daftar User Dukcapil Hadir di Desa' . '.pdf');
