<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN SURAT PINDAH YANG SUDAH SELESAI</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nomor Hp</td>
                                <td>Nama Pelapor</td>
                                <td>Tanggal Pengajuan</td>
                                <td>Desa</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from surat_pindah where status='selesai' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $petugas = $d['petugas'];
                                $q = mysqli_query($connection, "SELECT nama_desa FROM tbl_users WHERE username='$petugas'");
                                while ($t = mysqli_fetch_assoc($q)) {
                                    $nama_desa = $t['nama_desa'];

                            ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d['noreg']; ?></td>
                                        <td><?= $d['no_hp'] ?></td>
                                        <td><?= $d['nama_pelapor'] ?></td>
                                        <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                        <td><?= $nama_desa ?></td>
                                        <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>

                                            <!-- BEGIN  modal detail -->
                                            <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <?php
                                                        $id = $d['id'];
                                                        $query2 = "SELECT * FROM surat_pindah WHERE id='$id'";
                                                        $result = mysqli_query($connection, $query2);
                                                        while ($row2 = mysqli_fetch_assoc($result)) {
                                                        ?>

                                                            <div class="modal-header">
                                                                <h2 class="modal-title">Detail Surat Pindah <?= $row2['nama_pelapor'] ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <label class="form-label">Tanggal Pengajuan</label>
                                                                    <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($d['tgl_pengajuan'])) ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Email Desa</label>
                                                                    <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP Desa</label>
                                                                    <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>DATA PELAPOR</h3>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">NIK Pelapor</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nik_pelapor'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Pelapor</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_pelapor'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Alamat Lama Pelapor</label>
                                                                    <textarea id="" cols="30" rows="10" class="form-control" readonly><?= $row2['alamat_old'] ?></textarea>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Alamat Baru Pelapor</label>
                                                                    <textarea name="" id="" cols="30" rows="10" class="form-control" readonly><?= $row2['alamat_new'] ?></textarea>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>BERKAS PERSYARATAN</h3>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="../file desa/surat pindah/<?php echo $row2['ktp']; ?>">KTP Pelapor</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="../file desa/surat pindah/<?php echo $row2['kk']; ?>">KK Pelapor</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="../file desa/surat pindah/<?php echo $row2['surat_desa']; ?>">Surat Pengantar dari Desa</a>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<?php
include 'footer.php';
?>