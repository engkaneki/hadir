<?php
include 'header.php';
?>


<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DAFTAR PENGGUNA</h4>
                <p class="card-description">
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#tambah">Tambah Pengguna</button>
                    <a href="print_users.php" target="_blank" class="btn btn-success"><i class="fa fa-print"></i> &nbsp Print</a>
                </p>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nama</td>
                                <td>Nomor Hp</td>
                                <td>Email</td>
                                <td>Instansi</td>
                                <td>Jumlah Berkas</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from tbl_users");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $user = $d['username'];
                                $status = "ditolak";

                                $aktalahir = mysqli_query($connection, "SELECT * FROM akte_baru_lahir WHERE petugas='$user' AND status!='$status'");
                                $jumlahaktalahir = mysqli_num_rows($aktalahir);

                                $aktamati = mysqli_query($connection, "SELECT * FROM akte_mati WHERE petugas='$user' AND status!='$status'");
                                $jumlahaktamati = mysqli_num_rows($aktamati);

                                $kk = mysqli_query($connection, "SELECT * FROM kk WHERE petugas='$user' AND status!='$status'");
                                $jumlahkk = mysqli_num_rows($kk);

                                $suratpindah = mysqli_query($connection, "SELECT * FROM surat_pindah WHERE petugas='$user' AND status!='$status'");
                                $jumlahsuratpindah = mysqli_num_rows($suratpindah);

                                $berkas = $jumlahaktalahir + $jumlahaktamati + $jumlahkk + $jumlahsuratpindah;


                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td>
                                        <a data-toggle="modal" data-target="#detail<?= $no ?>">
                                            <?= $d['nama_pengguna']; ?>
                                        </a>
                                    </td>
                                    <td><?= $d['no_hp'] ?></td>
                                    <td><?= $d['email'] ?></td>
                                    <td><?= $d['nama_desa'] ?></td>
                                    <td><?= $berkas ?></td>
                                    <td>
                                        <button class="btn btn-secondary" data-toggle="modal" data-target="#edit<?= $no ?>">Edit</button>
                                        <button class="btn btn-warning" data-toggle="modal" data-target="#reset<?= $no ?>">Reset</button>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#hapus<?= $no ?>">Hapus</button>

                                        <!-- BEGIN modal edit -->
                                        <div class="modal fade" id="edit<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form action="scripts/function_user.php?act=edituser&id=<?= $d['id_user'] ?>" method="POST" enctype="multipart/form-data">

                                                        <?php
                                                        $id = $d['id_user'];
                                                        $query2 = "SELECT * FROM tbl_users WHERE id_user='$id'";
                                                        $result = mysqli_query($connection, $query2);
                                                        while ($row2 = mysqli_fetch_assoc($result)) {
                                                        ?>

                                                            <div class="modal-header">
                                                                <h2 class="modal-title">Detail Akun <?= $row2['nama_pengguna'] ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <label class="form-label">Username</label>
                                                                    <input type="text" class="form-control" name="username" value="<?= $row2['username'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Lengkap</label>
                                                                    <input type="text" class="form-control" name="nama_pengguna" value="<?= $row2['nama_pengguna'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Desa</label>
                                                                    <input type="text" class="form-control" name="nama_desa" value="<?= $row2['nama_desa'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Role</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['role'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Tugas</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['tugas'] ?>" readonly>
                                                                </div>
                                                                <div class="from-group row">
                                                                    <label class="form-label">Operator Capil</label>
                                                                    <select name="area" class="form-control">
                                                                        <option value="">Pilih Operator</option>
                                                                        <?php
                                                                        $sql = mysqli_query($connection, "SELECT * FROM tbl_users WHERE role='petugas'");
                                                                        while ($operator = mysqli_fetch_assoc($sql)) {
                                                                        ?>
                                                                            <option value="<?= $operator['username'] ?>"><?= $operator['nama_pengguna'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-secondary">Edit</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- END modal edit -->

                                        <!-- BEGIN  modal detail -->
                                        <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form method="POST" enctype="multipart/form-data">

                                                        <?php
                                                        $id = $d['id_user'];
                                                        $query2 = "SELECT * FROM tbl_users WHERE id_user='$id'";
                                                        $result = mysqli_query($connection, $query2);
                                                        while ($row2 = mysqli_fetch_assoc($result)) {
                                                        ?>

                                                            <div class="modal-header">
                                                                <h2 class="modal-title">Detail Akun <?= $row2['nama_pengguna'] ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <img src="../img/avatars/<?php echo $row2['photo']; ?>" class="img-fluid rounded-circle mb-2" alt="<?php echo $row['nama_pengguna'] ?>" />
                                                                <div class="form-group row">
                                                                    <label class="form-label">username</label>
                                                                    <input type="text" class="form-control" name="tgl_pengajuan" value="<?= $row2['username'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Lengkap</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_pengguna'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Email</label>
                                                                    <input type="email" class="form-control" value="<?= $row2['email'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Kades</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_kades'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Desa</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_desa'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Alamat Kantor</label>
                                                                    <textarea name="" id="" cols="30" rows="10" class="form-control" readonly><?= $row2['alamat'] ?></textarea>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Jabatan</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['tugas'] ?>" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal detail -->


                                        <!-- modal reset -->
                                        <div class="modal fade" id="reset<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h2 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                            reset
                                                            password</h2>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 align="center">Reset Password atas nama <?= $d['nama_pengguna']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <a href="scripts/function_user.php?act=resetpassword&id=<?= $d['id_user']; ?>" class="btn btn-warning">Reset</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal proses -->

                                        <!-- modal delete -->
                                        <div class="modal fade" id="hapus<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h2 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                            hapus
                                                            pengguna</h2>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 align="center">Menghapus Pengguna atas nama <?= $d['nama_pengguna']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <a href="scripts/function_user.php?act=deleteuser&id=<?= $d['id_user']; ?>" class="btn btn-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal delete -->

                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<!-- BEGIN  modal buat akte kelahiran -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_user.php?act=adduser" method="POST">
                <div class="modal-header">
                    <h2 class="modal-title">Tambah Pengguna Baru</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Username</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_pengguna" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Desa</label>
                        <input type="text" class="form-control" name="nama_desa">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Role</label>
                        <select name="role" id="" class="form-control">
                            <option value="desa">Admin Desa</option>
                            <option value="petugas">Admin Dukcapil</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Tugas</label>
                        <select name="tugas" id="" class="form-control">
                            <option value="Admin Desa">Admin Desa</option>
                            <option value="Admin Dukcapil">Admin Dukcapil</option>
                            <option value="Superuser">Superuser</option>
                        </select>
                    </div>
                    <div class="from-group row">
                        <label class="form-label">Operator Capil</label>
                        <select name="area" class="form-control">
                            <option value="">Pilih Operator</option>
                            <?php
                            $sql = mysqli_query($connection, "SELECT * FROM tbl_users WHERE role='petugas'");
                            while ($operator = mysqli_fetch_assoc($sql)) {
                            ?>
                                <option value="<?= $operator['username'] ?>"><?= $operator['nama_pengguna'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat akte kelahiran -->






<?php
include 'footer.php';
?>