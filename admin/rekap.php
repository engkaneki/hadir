<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA REKAP LAYANAN DESA PER BULAN</h4>
                <form action="" method="get">
                    <select name="bulan" class="form-select">
                        <option value="">Pilih Bulan</option>
                        <?php
                        $bulan_default = date("n"); // Mendapatkan bulan saat ini (tanpa angka nol di depan)
                        $daftar_bulan = array(
                            1 => "Januari", 2 => "Februari", 3 => "Maret",
                            4 => "April", 5 => "Mei", 6 => "Juni",
                            7 => "Juli", 8 => "Agustus", 9 => "September",
                            10 => "Oktober", 11 => "November", 12 => "Desember"
                        );

                        foreach ($daftar_bulan as $bulan_num => $bulan_nama) {
                            // Periksa apakah nilai bulan dipilih sesuai dengan nilai yang ada di $_GET
                            $selected = ($_GET['bulan'] == $bulan_num) ? "selected" : "";
                            echo "<option value='$bulan_num' $selected>$bulan_nama</option>";
                        }
                        ?>
                    </select>

                    <select name="tahun" class="form-select">
                        <option value="">Pilih Tahun</option>
                        <?php
                        $tahun_default = date("Y"); // Mendapatkan tahun saat ini
                        $tahun_minimum = 2021; // Tahun minimum

                        foreach (range($tahun_minimum, $tahun_default) as $tahun) {
                            // Periksa apakah nilai tahun dipilih sesuai dengan nilai yang ada di $_GET
                            $selected = ($_GET['tahun'] == $tahun) ? "selected" : "";
                            echo "<option value='$tahun' $selected>$tahun</option>";
                        }
                        ?>
                    </select>

                    <input type="submit" class="btn btn-info" value="Tampilkan Data">
                    <a href="cetakrekap.php?bulan=<?php echo $_GET['bulan']; ?>&tahun=<?php echo $_GET['tahun']; ?>" class="btn btn-success">Cetak</a>
                </form>

                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <!-- Bagian header tabel -->
                        <thead>
                            <tr>
                                <td rowspan="2" align="center">No</td>
                                <td rowspan="2" align="center">Desa</td>
                                <td colspan="4" align="center">Layanan</td>
                            </tr>
                            <tr>
                                <td align="middle">Kartu Keluarga</td>
                                <td align="center">Akta Kelahiran</td>
                                <td align="center">Akta Kematian</td>
                                <td align="center">Surat Pindah</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include 'scripts/koneksi.php';
                            $no = 1;
                            $bulan_terpilih = isset($_GET['bulan']) ? $_GET['bulan'] : $bulan_default; // Ambil bulan yang dipilih dari input select atau gunakan bulan saat ini
                            $tahun_terpilih = isset($_GET['tahun']) ? $_GET['tahun'] : $tahun_default; // Ambil tahun yang dipilih dari input select atau gunakan tahun saat ini

                            // Ubah format bulan dan tahun terpilih menjadi 'YYYY-MM' untuk penggunaan dalam SQL
                            $bulan_tahun_terpilih = $tahun_terpilih . '-' . str_pad($bulan_terpilih, 2, '0', STR_PAD_LEFT);

                            $d = mysqli_query($connection, "SELECT * FROM tbl_users WHERE role='desa'");
                            while ($desa = mysqli_fetch_assoc($d)) {
                                $namadesa = $desa['nama_desa'];
                                $username = $desa['username'];
                                $kk = mysqli_query($connection, "SELECT * FROM kk WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
                                $akta_lahir = mysqli_query($connection, "SELECT * FROM akte_baru_lahir WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
                                $akta_mati = mysqli_query($connection, "SELECT * FROM akte_mati WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
                                $surat_pindah = mysqli_query($connection, "SELECT * FROM surat_pindah WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
                                $jumlah_kk = mysqli_num_rows($kk);
                                $jumlah_lahir = mysqli_num_rows($akta_lahir);
                                $jumlah_mati = mysqli_num_rows($akta_mati);
                                $jumlah_pindah = mysqli_num_rows($surat_pindah);
                            ?>
                                <tr>
                                    <td>
                                        <?php echo $no++; ?>
                                    </td>
                                    <td>
                                        <?php echo $namadesa; ?>
                                    </td>
                                    <td><?php echo $jumlah_kk; ?></td>
                                    <td><?php echo $jumlah_lahir; ?></td>
                                    <td><?php echo $jumlah_mati; ?></td>
                                    <td><?php echo $jumlah_pindah; ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<!-- End of Main Content-->


<?php
include 'footer.php';
?>