<?php
// Mengambil nilai bulan dan tahun dari query string
$bulan = $_GET['bulan'];
$tahun = $_GET['tahun'];

// Validasi data bulan dan tahun
if (empty($bulan) || empty($tahun)) {
    die("Pilih bulan dan tahun terlebih dahulu.");
}

// Sambungkan ke database
include 'scripts/koneksi.php';

// Query data sesuai dengan bulan dan tahun yang dipilih
$bulan_tahun_terpilih = $tahun . '-' . str_pad($bulan, 2, '0', STR_PAD_LEFT);

// Mengambil data dari tabel sesuai dengan bulan dan tahun yang dipilih
$sql = "SELECT * FROM tbl_users WHERE role='desa'";
$result = mysqli_query($connection, $sql);

// Mengatur header untuk hasil cetak
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=data_cetak_$bulan-$tahun.xls");

// Memulai tabel Excel
echo "<table border='1'>";
echo "<thead>";
echo "<tr>";
echo "<th>No</th>";
echo "<th>Desa</th>";
echo "<th>Kartu Keluarga</th>";
echo "<th>Akta Kelahiran</th>";
echo "<th>Akta Kematian</th>";
echo "<th>Surat Pindah</th>";
echo "</tr>";
echo "</thead>";
echo "<tbody>";

$no = 1;
$total_kk = 0;
$total_lahir = 0;
$total_mati = 0;
$total_pindah = 0;

while ($desa = mysqli_fetch_assoc($result)) {
    $namadesa = $desa['nama_desa'];
    $username = $desa['username'];

    // Query data sesuai dengan bulan dan tahun yang dipilih
    $kk = mysqli_query($connection, "SELECT * FROM kk WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
    $akta_lahir = mysqli_query($connection, "SELECT * FROM akte_baru_lahir WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
    $akta_mati = mysqli_query($connection, "SELECT * FROM akte_mati WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
    $surat_pindah = mysqli_query($connection, "SELECT * FROM surat_pindah WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_tahun_terpilih'");
    $jumlah_kk = mysqli_num_rows($kk);
    $jumlah_lahir = mysqli_num_rows($akta_lahir);
    $jumlah_mati = mysqli_num_rows($akta_mati);
    $jumlah_pindah = mysqli_num_rows($surat_pindah);

    // Menampilkan data dalam tabel Excel
    echo "<tr>";
    echo "<td>$no</td>";
    echo "<td>$namadesa</td>";
    echo "<td>$jumlah_kk</td>";
    echo "<td>$jumlah_lahir</td>";
    echo "<td>$jumlah_mati</td>";
    echo "<td>$jumlah_pindah</td>";
    echo "</tr>";

    // Menghitung total data
    $total_kk += $jumlah_kk;
    $total_lahir += $jumlah_lahir;
    $total_mati += $jumlah_mati;
    $total_pindah += $jumlah_pindah;

    $no++;
}

// Tampilkan total data di bawah tabel
echo "<tr>";
echo "<td colspan='2'><strong>Total</strong></td>";
echo "<td><strong>$total_kk</strong></td>";
echo "<td><strong>$total_lahir</strong></td>";
echo "<td><strong>$total_mati</strong></td>";
echo "<td><strong>$total_pindah</strong></td>";
echo "</tr>";

echo "</tbody>";
echo "</table>";

// Tutup koneksi database
mysqli_close($connection);
