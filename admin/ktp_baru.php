<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN CETAK KTP</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>NIK</td>
                                <td>Nama Pelapor</td>
                                <td>Tanggal Pengajuan</td>
                                <td>Desa</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $data = mysqli_query($connection, "select * from ktp where status='pending' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $petugas = $d['petugas'];
                                $q = mysqli_query($connection, "SELECT * FROM tbl_users WHERE username='$petugas'");
                                while ($t = mysqli_fetch_assoc($q)) {
                                    $nama_desa = $t['nama_desa'];
                                    $email = $t['email'];
                                    $no_hp = $t['no_hp'];

                            ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d['nik'] ?></td>
                                        <td><?= $d['nama'] ?></td>
                                        <td><?= format_tanggal_indonesia($d["tanggal_pengajuan"], 'tanggal_bulan_tahun'); ?>
                                        </td>
                                        <td><?= $nama_desa ?></td>
                                        <td>
                                            <button class="btn btn-secondary" data-toggle="modal" data-target="#proses<?= $no ?>">Proses</button>



                                            <!-- modal proses -->
                                            <div class="modal fade" id="proses<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <form action="scripts/function_desa.php?act=prosesktp&id=<?= $d['id']; ?>" method="POST">

                                                            <div class="modal-header">
                                                                <h2 class="modal-title" id="exampleModalLabel">Proses cetak KTP
                                                                    atas nama <?= $d['nama']; ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <input class="form-control" name="id" type="text" value="<?= $d['id'] ?>" hidden="true">
                                                                    <input class="form-control" name="nik" type="text" value="<?= $d['nik'] ?>" hidden="true">
                                                                    <input class="form-control" name="petugas" type="text" value="<?= $d['petugas'] ?>" hidden="true">
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Pelapor</label>
                                                                    <input type="text" name="nama" class="form-control" value="<?= $d['nama'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Status Laporan</label>
                                                                    <select name="status" class="form-control">
                                                                        <option value="selesai">Selesai</option>
                                                                        <option value="ditolak">Ditolak</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Isi keterangan jika pengajuan
                                                                        ditolak</label>
                                                                    <textarea class="form-control" name="ket" id="" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                                <button type="submit" class="btn btn-secondary">Proses</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- modal proses -->



                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<!-- End of Main Content-->

<?php
include 'footer.php';
?>