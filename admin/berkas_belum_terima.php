<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA BERKAS YANG BELUM DI TERIMA</h4>
                <a href="print_laporan.php" target="_blank" class="btn btn-success"><i class="fa fa-print"></i> &nbsp Print</a>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>NIK Pelapor</td>
                                <td>Nama Pelapor</td>
                                <td>Jenis Berkas</td>
                                <td>Desa</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $data = mysqli_query($connection, "select * from berkas where status='selesai' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $petugas = $d['petugas'];
                                $q = mysqli_query($connection, "SELECT * FROM tbl_users WHERE username='$petugas'");
                                while ($t = mysqli_fetch_assoc($q)) {
                                    $nama_desa = $t['nama_desa'];
                                    $email = $t['email'];
                                    $no_hp = $t['no_hp'];


                            ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d['noreg']; ?></td>
                                        <td><?= $d['nik'] ?></td>
                                        <td><?= $d['nama'] ?></td>
                                        <td><?= $d['jenis_berkas'] ?></td>
                                        <td><?= $nama_desa ?></td>
                                        <td>
                                            <button class="btn btn-secondary" data-toggle="modal" data-target="#proses<?= $no ?>">Terima</button>

                                            <div class="modal fade" id="proses<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi terima berkas</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 align="center">Terima berkas atas nama <?= $d['nama']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                            <a href="scripts/function_desa.php?act=terimaberkas&id=<?= $d['id']; ?>" class="btn btn-secondary">Terima</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- modal delete -->



                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->

<?php
include 'footer.php';
?>