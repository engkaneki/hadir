<?php
require('library/fpdf.php');
include 'scripts/koneksi.php';

function tanggal_indonesia($tanggal)
{

    $bulan = array(
        1 =>    'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    $var = explode('-', $tanggal);

    return $var[2] . ' ' . $bulan[(int)$var[1]] . ' ' . $var[0];
}

$pdf = new FPDF('P', 'mm', 'A4');
$pdf->AddPage();

$pdf->SetFont('Times', 'B', 14);
$pdf->Cell(200, 10, 'DAFTAR BERKAS DUKCAPIL HADIR DI DESA', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 1);
$pdf->SetFont('Times', 'B', 14);
$pdf->Cell(200, 10, 'YANG BELUM DI TERIMA', 0, 0, 'C');


$pdf->Cell(5, 15, '', 0, 1);
$pdf->SetFont('Times', 'B', 9);
$pdf->Cell(35, 7, 'NO REGISTRASI', 1, 0, 'C');
$pdf->Cell(25, 7, 'NIK', 1, 0, 'C');
$pdf->Cell(55, 7, 'NAMA PELAPOR', 1, 0, 'C');
$pdf->Cell(35, 7, 'JENIS BERKAS', 1, 0, 'C');
$pdf->Cell(40, 7, 'DESA', 1, 0, 'C');


$pdf->Cell(5, 7, '', 0, 1);
$pdf->SetFont('Times', '', 7);
$data = mysqli_query($connection, "SELECT  * FROM berkas WHERE status='selesai' order by petugas asc");
while ($d = mysqli_fetch_array($data)) {
    $petugas = $d['petugas'];
    $dat = mysqli_query($connection, "SELECT * FROM tbl_users WHERE username='$petugas'");
    while ($d2 = mysqli_fetch_array($dat)) {
        $desa = $d2['nama_desa'];

        $pdf->Cell(35, 10, $d['noreg'], 1, 0, 'C');
        $pdf->Cell(25, 10, $d['nik'], 1, 0);
        $pdf->Cell(55, 10, $d['nama'], 1, 0);
        $pdf->Cell(35, 10, $d['jenis_berkas'], 1, 0);
        $pdf->Cell(40, 10, $desa, 1, 1);
    }
}

$pdf->Output('', 'Laporan Berkas Hadir di Desa' . '.pdf');
