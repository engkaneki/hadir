<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN AKTE KEMATIAN</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nomor Hp</td>
                                <td>Nama Pelapor</td>
                                <td>Nama Keluarga Yang Meninggal</td>
                                <td>Tanggal Pengajuan</td>
                                <td>Desa</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from akte_mati where status='pending' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $petugas = $d['petugas'];
                                $q = mysqli_query($connection, "SELECT nama_desa FROM tbl_users WHERE username='$petugas'");
                                while ($t = mysqli_fetch_assoc($q)) {
                                    $nama_desa = $t['nama_desa'];

                            ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d['noreg']; ?></td>
                                        <td><?= $d['no_hp'] ?></td>
                                        <td><?= $d['nama_pelapor'] ?></td>
                                        <td><?= $d['nama_mati'] ?></td>
                                        <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                        <td><?= $nama_desa ?></td>
                                        <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>
                                            <button class="btn btn-secondary" data-toggle="modal" data-target="#proses<?= $no ?>">Proses</button>

                                            <!-- BEGIN  modal detail -->
                                            <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <form action="scripts/function_desa.php?act=prosesaktemati" method="POST" enctype="multipart/form-data">

                                                            <?php
                                                            $id = $d['id'];
                                                            $query2 = "SELECT * FROM akte_mati WHERE id='$id'";
                                                            $result = mysqli_query($connection, $query2);
                                                            while ($row2 = mysqli_fetch_assoc($result)) {
                                                            ?>

                                                                <div class="modal-header">
                                                                    <h2 class="modal-title">Detail Akte Kematian <?= $row2['nama_mati'] ?></h2>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body m-3">
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Tanggal Pengajuan</label>
                                                                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($d['tgl_pengajuan'])) ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Email Desa</label>
                                                                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">No HP Desa</label>
                                                                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                    </div>
                                                                    <div>
                                                                        <hr class="dropdown-divider">
                                                                    </div>
                                                                    <h3>DATA PELAPOR</h3>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">No HP</label>
                                                                        <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">NIK Pelapor</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nik_pelapor'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Nama Pelapor</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nama_pelapor'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">NIK Keluarga Yang Meninggal</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nik_mati'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Nama Keluarga Yang Meninggal</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nama_mati'] ?>" readonly>
                                                                    </div>
                                                                    <div>
                                                                        <hr class="dropdown-divider">
                                                                    </div>
                                                                    <h3>BERKAS PERSYARATAN</h3>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['ktp_pelapor']; ?>">KTP Pelapor</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['ktp_mati']; ?>">KTP Keluarga yang Meninggal</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['kk']; ?>">KK Keluarga yang Meninggal</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['surat_kuning']; ?>">Surat Kuning</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['surat_mati_desa']; ?>">Surat Keterangan Desa</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte mati/<?php echo $row2['formulir_mati']; ?>">Surat Keterangan Desa</a>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- modal proses -->
                                            <div class="modal fade" id="proses<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <form action="scripts/function_desa.php?act=prosesaktemati&id=<?= $d['id']; ?>" method="POST" enctype="multipart/form-data">

                                                            <div class="modal-header">
                                                                <h2 class="modal-title" id="exampleModalLabel">Proses akte kematian atas nama <?= $d['nama_mati']; ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <input class="form-control" name="id" type="text" value="<?= $d['id'] ?>" hidden="true">
                                                                    <label class="form-label">No Registrasi</label>
                                                                    <input type="text" class="form-control" value="<?= $d['noreg'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Keluarga yang Meninggal</label>
                                                                    <input type="text" class="form-control" value="<?= $d['nama_mati'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Status Laporan</label>
                                                                    <select name="status" class="form-control">
                                                                        <option value="selesai">Selesai</option>
                                                                        <option value="ditolak">Ditolak</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Isi keterangan jika pengajuan ditolak</label>
                                                                    <textarea class="form-control" name="ket" id="" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                                <button type="submit" class="btn btn-secondary">Proses</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- modal proses -->

                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->

<?php
include 'footer.php';
?>