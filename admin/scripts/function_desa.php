<?php

include 'koneksi.php';

if ($_GET['act'] == 'proseskk') {
    $id = $_GET['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $jenis_berkas = "Kartu Keluarga";
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kk SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kk SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'proseskkrusak') {
    $id = $_GET['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $jenis_berkas = "Kartu Keluarga Hilang/Rusak";
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kkrusak SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kk SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'proseskkubah') {
    $id = $_GET['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $jenis_berkas = "Kartu Keluarga Perubahan Data";
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kkubah SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE kk SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            header("location: ../kk_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'prosesaktelahir') {
    $id = $_POST['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jenis_berkas = "Akta Kelahiran";
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE akte_baru_lahir SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            # credirect ke page index
            header("location: ../akte_lahir.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE akte_baru_lahir SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            # credirect ke page index
            header("location: ../akte_lahir.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'prosesaktemati') {
    $id = $_POST['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jenis_berkas = "Akta Kematian";
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE akte_mati SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            # credirect ke page index
            header("location: ../akte_kematian.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE akte_mati SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            # credirect ke page index
            header("location: ../akte_kematian.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'prosespindah') {
    $id = $_POST['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $noreg = $_POST['noreg'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jenis_berkas = "Surat Pindah";
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('$noreg', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE surat_pindah SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            # credirect ke page index
            header("location: ../surat_pindah.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE surat_pindah SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            # credirect ke page index
            header("location: ../surat_pindah.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'prosesktp') {
    $id = $_POST['id'];
    $status = $_POST['status'];
    $ket = $_POST['ket'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jenis_berkas = "Cetak KTP";
    $petugas = $_POST['petugas'];

    if ($status == 'selesai') {
        //simpan ke tabel berkas
        $addberkas = mysqli_query($connection, "INSERT INTO berkas(noreg, nik, nama, jenis_berkas, status, petugas) VALUES('ktp', '$nik', '$nama', '$jenis_berkas', '$status', '$petugas')");

        //query update
        $queryupdate = mysqli_query($connection, "UPDATE ktp SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($addberkas and $queryupdate) {
            # credirect ke page index
            header("location: ../ktp_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    } else {
        //query update
        $queryupdate = mysqli_query($connection, "UPDATE ktp SET status='$status' , ket='$ket' WHERE id='$id' ");

        if ($queryupdate) {
            # credirect ke page index
            header("location: ../ktp_baru.php");
        } else {
            echo "ERROR, data gagal diupdate" . mysqli_error($connection);
        }
    }
} else if ($_GET['act'] == 'terimaberkas') {
    $id = $_GET['id'];
    $status = "terima";

    //query update
    $queryupdate = mysqli_query($connection, "UPDATE berkas SET status='$status' WHERE id='$id'");

    if ($queryupdate) {
        # redirect ke page index
        header("location: ../berkas_belum_terima.php");
    } else {
        echo "ERROR, data gagal diupdate" . mysqli_error($connection);
    }
}
