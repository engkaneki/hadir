<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DUKCAPIL HADIR di DESA</title>
    <link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
</head>

<body>
    <img src="img/wave.png" alt="" class="wave">
    <div class="container">
        <div class="img">
            <img src="img/logo.png" alt="">
        </div>
        <div class="login-container">
            <form>
                <h2>Selamat Datang</h2>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div>
                        <h5>Username</h5>
                        <input id="username" type="text" class="input">
                    </div>
                </div>
                <div class="input-div two">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div>
                        <h5>Password</h5>
                        <input id="password" type="password" class="input">
                    </div>
                </div>
                <button type="button" class="btn btn-login">
                    Login
                </button>
            </form>
        </div>
    </div>
    <script src="js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function() {

            $(".btn-login").click(function() {

                var username = $("#username").val();
                var password = $("#password").val();

                if (username.length == "") {

                    Swal.fire({
                        type: 'warning',
                        title: 'Oops...',
                        text: 'Username Wajib Diisi !'
                    });

                } else if (password.length == "") {

                    Swal.fire({
                        type: 'warning',
                        title: 'Oops...',
                        text: 'Password Wajib Diisi !'
                    });

                } else {

                    $.ajax({

                        url: "scripts/verification-login.php",
                        type: "POST",
                        data: {
                            "username": username,
                            "password": password
                        },

                        success: function(response) {

                            if (response == "petugas") {

                                Swal.fire({
                                        type: 'success',
                                        title: 'Login Berhasil!',
                                        text: '',
                                        timer: 2000,
                                        showCancelButton: false,
                                        showConfirmButton: false
                                    })
                                    .then(function() {
                                        window.location.href =
                                            "pelayanan/";
                                    });

                            } else if (response == "desa") {

                                Swal.fire({
                                        type: 'success',
                                        title: 'Login Berhasil!',
                                        text: '',
                                        timer: 2000,
                                        showCancelButton: false,
                                        showConfirmButton: false
                                    })
                                    .then(function() {
                                        window.location.href =
                                            "./";

                                        // "./";
                                    });

                            } else if (response == "admin") {

                                Swal.fire({
                                        type: 'success',
                                        title: 'Login Berhasil!',
                                        text: '',
                                        timer: 2000,
                                        showCancelButton: false,
                                        showConfirmButton: false
                                    })
                                    .then(function() {
                                        window.location.href =
                                            "admin/";
                                    });
                            } else if (response == "belum") {

                                Swal.fire({
                                    type: 'error',
                                    title: 'Mohon Segera Menyampaikan Surat Penunjukkan Operator!'
                                });

                            } else {

                                Swal.fire({
                                    type: 'error',
                                    title: 'Login Gagal!',
                                    text: 'silahkan coba lagi!'
                                });

                            }

                            console.log(response);

                        },

                        error: function(response) {

                            Swal.fire({
                                type: 'error',
                                title: 'Opps!',
                                text: 'server error!'
                            });

                            console.log(response);

                        }

                    });

                }

            });

        });
    </script>
    <!-- endinject -->
</body>

</html>