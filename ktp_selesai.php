<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN CETAK KTP YANG DITOLAK</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Hp</td>
                                <td>Nama Pelapor</td>
                                <td>Tanggal Pengajuan</td>
                                <td>Keterangan</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from ktp where petugas='$username' AND status='selesai' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {

                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $d['no_hp'] ?></td>
                                    <td><?= $d['nama'] ?></td>
                                    <td><?= format_tanggal_indonesia($d["tanggal_pengajuan"], 'tanggal_bulan_tahun'); ?>
                                    </td>
                                    <td><?= $d['ket'] ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<?php
include 'footer.php';
?>