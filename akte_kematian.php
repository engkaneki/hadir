<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN AKTE KEMATIAN</h4>
                <p class="card-description">
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#aktemati">Buat Akte Kematian</button>
                </p>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nomor Hp</td>
                                <td>Nama Pelapor</td>
                                <td>Nama Keluarga Yang Meninggal</td>
                                <td>Tanggal Pengajuan</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from akte_mati where petugas='$username' AND status='pending' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {

                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $d['noreg']; ?></td>
                                    <td><?= $d['no_hp'] ?></td>
                                    <td><?= $d['nama_pelapor'] ?></td>
                                    <td><?= $d['nama_mati'] ?></td>
                                    <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                    <td>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#hapusmati<?= $no ?>">Hapus</button>

                                        <!-- BEGIN  modal detail -->
                                        <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form action="scripts/function_desa.php?act=kkbaru" method="POST" enctype="multipart/form-data">

                                                        <?php
                                                        $id = $d['id'];
                                                        $query2 = "SELECT * FROM akte_mati WHERE id='$id'";
                                                        $result = mysqli_query($connection, $query2);
                                                        while ($row2 = mysqli_fetch_assoc($result)) {
                                                        ?>

                                                            <div class="modal-header">
                                                                <h2 class="modal-title">Detail Akte Kematian <?= $row2['nama_mati'] ?></h2>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body m-3">
                                                                <div class="form-group row">
                                                                    <label class="form-label">Tanggal Pengajuan</label>
                                                                    <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($d['tgl_pengajuan'])) ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Email Desa</label>
                                                                    <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP Desa</label>
                                                                    <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>DATA PELAPOR</h3>
                                                                <div class="form-group row">
                                                                    <label class="form-label">No HP</label>
                                                                    <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">NIK Pelapor</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nik_pelapor'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Pelapor</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_pelapor'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">NIK Keluarga Yang Meninggal</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nik_mati'] ?>" readonly>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="form-label">Nama Keluarga Yang Meninggal</label>
                                                                    <input type="text" class="form-control" value="<?= $row2['nama_mati'] ?>" readonly>
                                                                </div>
                                                                <div>
                                                                    <hr class="dropdown-divider">
                                                                </div>
                                                                <h3>BERKAS PERSYARATAN</h3>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['ktp_pelapor']; ?>">KTP Pelapor</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['ktp_mati']; ?>">KTP Keluarga yang Meninggal</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['kk']; ?>">KK Keluarga yang Meninggal</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['surat_kuning']; ?>">Surat Kuning</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['surat_mati_desa']; ?>">Surat Keterangan Desa</a>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <a class="btn btn-info" target="_blank" href="file desa/akte mati/<?php echo $row2['formulir_mati']; ?>">SPTJM Kematian</a>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- modal delete -->
                                        <div class="modal fade" id="hapusmati<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                            hapus
                                                            pengajuan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 align="center">Menghapus Pengajuan atas nama <?= $d['nama_pelapor']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <a href="scripts/function_desa.php?act=deletemati&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal delete -->

                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<!-- BEGIN  modal buat akte kematian -->
<div class="modal fade" id="aktemati" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=aktemati" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Buat Akte Kematian</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Pelapor</label>
                        <input type="text" class="form-control" name="nik_pelapor" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Pelapor</label>
                        <input type="text" class="form-control" name="nama_pelapor" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Keluarga Yang Meninggal</label>
                        <input type="text" class="form-control" name="nik_mati" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Keluarga Yang Meninggal</label>
                        <input type="text" class="form-control" name="nama_mati" required>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label class="form-label">KTP Pelapor</label>
                        <input type="file" class="form-control" name="ktp_pelapor" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KTP Keluarga yang Meninggal</label>
                        <input type="file" class="form-control" name="ktp_mati" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Kartu Keluarga yang Meninggal</label>
                        <input type="file" class="form-control" name="kk" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Kematian dari Desa/Rumah Sakit (Surat Kuning)</label>
                        <input type="file" class="form-control" name="surat_kuning" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Keterangan Kematian dari Desa/Lurah</label>
                        <input type="file" class="form-control" name="surat_mati_desa" required>
                        <input type="text" name="petugas" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">SPTJM Kematian</label>
                        <input type="file" class="form-control" name="formulir_mati" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat akte kematian -->





<?php
include 'footer.php';
?>