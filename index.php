<?php
include 'header.php';
include 'scripts/koneksi.php';
$username = $row['username'];

$query = "SELECT 
            (SELECT COUNT(*) FROM akte_baru_lahir WHERE petugas='$username' AND status='pending') as jumlah_akte_lahir,
            (SELECT COUNT(*) FROM akte_mati WHERE petugas='$username' AND status='pending') as jumlah_akte_mati,
            (SELECT COUNT(*) FROM surat_pindah WHERE petugas='$username' AND status='pending') as jumlah_surat_pindah,
            (SELECT COUNT(*) FROM kk WHERE petugas='$username' AND status='pending') as jumlah_kkbr,
            (SELECT COUNT(*) FROM kkrusak WHERE petugas='$username' AND status='pending') as jumlah_kkrusak,
            (SELECT COUNT(*) FROM kkubah WHERE petugas='$username' AND status='pending') as jumlah_kkubah";
$result = mysqli_query($connection, $query);
$row = mysqli_fetch_assoc($result);

$jumlah_kk = $row['jumlah_kkbr'] + $row['jumlah_kkrusak'] + $row['jumlah_kkubah'];
$jumlah_akte_lahir = $row['jumlah_akte_lahir'];
$jumlah_akte_mati = $row['jumlah_akte_mati'];
$jumlah_surat_pindah = $row['jumlah_surat_pindah'];

// Menghitung jumlah data pada bulan ini
$bulan_ini = date("Y-m");
$nama_bulan_ini = date("F Y", strtotime($bulan_ini));
$query = "SELECT 
            (SELECT COUNT(*) FROM akte_baru_lahir WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data1,
            (SELECT COUNT(*) FROM akte_mati WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data2,
            (SELECT COUNT(*) FROM surat_pindah WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data3,
            (SELECT COUNT(*) FROM kk WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data4,
            (SELECT COUNT(*) FROM kkrusak WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data5,
            (SELECT COUNT(*) FROM kkubah WHERE petugas='$username' AND DATE_FORMAT(tgl_pengajuan, '%Y-%m') = '$bulan_ini') as total_data6";
$result = mysqli_query($connection, $query);
$row = mysqli_fetch_assoc($result);

// Menampilkan jumlah data dari keenam tabel pada bulan ini
$total_data = $row['total_data1'] + $row['total_data2'] + $row['total_data3'] + $row['total_data4'] + $row['total_data5'] + $row['total_data6'];

?>

<!-- Main Content-->


<div class="row">
  <div class="col-xl-6 col-xxl-7">
    <div class="card flex-fill w-100">
      <div class="card-header">

        <h5 class="card-title mb-0">Jumlah Berkas Pengajuan Bulan <?= format_tanggal_indonesia($bulan_ini, 'bulan_tahun'); ?> = <?= $total_data ?> Berkas</h5>
      </div>
      <div class="card-body py-3">
        <div class="chart chart-sm">
          <canvas id="chartjs-dashboard-line"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-xxl-5 d-flex">
    <div class="w-100">
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-0">
                  <h5 class="card-title">Pengajuan KK</h5>
                </div>

                <div class="col-auto">
                  <div class="avatar">
                    <div class="avatar-title rounded-circle bg-primary-dark">
                      <i class="align-middle" data-feather="file-plus"></i>
                    </div>
                  </div>
                </div>
              </div>
              <h1 class="display-5 mt-1 mb-3"><?= $jumlah_kk ?></h1>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-0">
                  <h5 class="card-title">
                    Pengajuan Akte Kelahiran
                  </h5>
                </div>

                <div class="col-auto">
                  <div class="avatar">
                    <div class="avatar-title rounded-circle bg-primary-dark">
                      <i class="align-middle" data-feather="user-plus"></i>
                    </div>
                  </div>
                </div>
              </div>
              <h1 class="display-5 mt-1 mb-3"><?= $jumlah_akte_lahir ?></h1>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-0">
                  <h5 class="card-title">
                    Pengajuan Akte Kematian
                  </h5>
                </div>

                <div class="col-auto">
                  <div class="avatar">
                    <div class="avatar-title rounded-circle bg-primary-dark">
                      <i class="align-middle" data-feather="user-minus"></i>
                    </div>
                  </div>
                </div>
              </div>
              <h1 class="display-5 mt-1 mb-3"><?= $jumlah_akte_mati ?></h1>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-0">
                  <h5 class="card-title">Pengajuan Surat Pindah</h5>
                </div>

                <div class="col-auto">
                  <div class="avatar">
                    <div class="avatar-title rounded-circle bg-primary-dark">
                      <i class="align-middle" data-feather="file-plus"></i>
                    </div>
                  </div>
                </div>
              </div>
              <h1 class="display-5 mt-1 mb-3"><?= $jumlah_surat_pindah ?></h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</main>
<!-- End of Main Content-->

<?php
include 'footer.php';
?>