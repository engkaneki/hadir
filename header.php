<?php
session_start();

if (!$_SESSION['id_user']) {
    header("location:login.php");
} else if ($_SESSION['role'] == "petugas") {
    header("location: pelayanan/");
} else if ($_SESSION['role'] == "admin") {
    header("location: admin/");
}

include 'scripts/koneksi.php';
$id_user = $_SESSION['id_user'];

$query = "SELECT * FROM tbl_users WHERE id_user=$id_user";
$result = mysqli_query($connection, $query);
$row = mysqli_fetch_array($result);
$username = $row['username'];

// Array yang berisi nama tabel
$tables = array(
    "akte_baru_lahir",
    "akte_mati",
    "kk",
    "kkrusak",
    "kkubah",
    "surat_pindah"
);

// Array yang berisi label bulan
$label = array();
for ($i = 0; $i < 12; $i++) {
    $timestamp = strtotime("-$i month");
    array_push($label, date("M Y", $timestamp));
}

$label = array_reverse($label);

// Loop untuk mengambil data jumlah dalam 12 bulan terakhir
$data = array();
foreach ($tables as $table) {
    if ($table == "kk" || $table == "kkrusak" || $table == "kkubah") {
        continue; // skip data KK, KK Rusak, dan KK Ubah
    }
    $jumlah = array();
    for ($i = 0; $i < 12; $i++) {
        $timestamp = strtotime("-$i month");
        $tahun = date("Y", $timestamp);
        $bulan = date("m", $timestamp);
        $query = "SELECT COUNT(*) FROM $table WHERE petugas='$username' AND YEAR(tgl_pengajuan) = $tahun AND MONTH(tgl_pengajuan) = $bulan";
        $result = mysqli_query($connection, $query);
        array_push($jumlah, mysqli_fetch_array($result)[0]);
    }
    $data[$table] = array_reverse($jumlah);
}

// Mengambil data jumlah KK, KK Rusak, dan KK Ubah dalam 12 bulan terakhir
$jumlah_kk = array();
for ($i = 0; $i < 12; $i++) {
    $timestamp = strtotime("-$i month");
    $tahun = date("Y", $timestamp);
    $bulan = date("m", $timestamp);
    $query_kk = "SELECT COUNT(*) FROM kk WHERE petugas='$username' AND YEAR(tgl_pengajuan) = $tahun AND MONTH(tgl_pengajuan) = $bulan";
    $query_kkrusak = "SELECT COUNT(*) FROM kkrusak WHERE petugas='$username' AND YEAR(tgl_pengajuan) = $tahun AND MONTH(tgl_pengajuan) = $bulan";
    $query_kkubah = "SELECT COUNT(*) FROM kkubah WHERE petugas='$username' AND YEAR(tgl_pengajuan) = $tahun AND MONTH(tgl_pengajuan) = $bulan";
    $result_kk = mysqli_query($connection, $query_kk);
    $result_kkrusak = mysqli_query($connection, $query_kkrusak);
    $result_kkubah = mysqli_query($connection, $query_kkubah);
    $jumlah_kk[$i] = mysqli_fetch_array($result_kk)[0] + mysqli_fetch_array($result_kkrusak)[0] + mysqli_fetch_array($result_kkubah)[0];
}
$data["kk"] = array_reverse($jumlah_kk);

$jumlah_akta_lahir = $data['akte_baru_lahir'];
$jumlah_akta_mati = $data['akte_mati'];
$jumlah_sp = $data['surat_pindah'];
$jumlah_kkk = $data["kk"];

function format_tanggal_indonesia($tanggal, $format = 'default')
{
    $bulan = array(
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    $var = explode('-', $tanggal);

    switch ($format) {
        case 'default':
            return $var[2] . ' ' . $bulan[(int)$var[1]] . ' ' . $var[0];
            break;
        case 'bulan_tahun':
            return $bulan[(int)$var[1]] . ' ' . $var[0];
            break;
        case 'tanggal_bulan_tahun':
            return $var[2] . ' ' . $bulan[(int)$var[1]] . ' ' . $var[0];
            break;
        default:
            return false;
            break;
    }
}

$tgl_hari_ini = date("Y-m-d");


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Aplikasi Dukcapil Hadir Di Desa" />
    <meta name="author" content="Anggu Surya" />
    <link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

    <title>DUKCAPIL HADIR DI DESA</title>

    <style>
    body {
        opacity: 0;
    }
    </style>
    <script src="js/settings.js"></script>

</head>

<body>
    <div class="splash active">
        <div class="splash-icon"></div>
    </div>

    <div class="wrapper">
        <nav id="sidebar" class="sidebar">
            <a class="sidebar-brand" href="index.php">
                <center>
                    <img src="img/logo.png" height="100px" alt="Home">
                </center>
            </a>
            <div class="sidebar-content">
                <div class="sidebar-user">
                    <img src="img/avatars/<?php echo $row['photo']; ?>" class="img-fluid rounded-circle mb-2"
                        alt="<?php echo $row['nama_pengguna'] ?>" />
                    <div>
                        <a href="#" data-toggle="modal" data-target="#modalgantifoto">ganti foto</a>
                    </div>
                    <div class="font-weight-bold"><?php echo $row['nama_pengguna']; ?></div>
                    <small><?php echo $row['tugas']; ?></small>
                </div>

                <ul class="sidebar-nav">
                    <li class="sidebar-header">Main</li>
                    <li class="sidebar-item">
                        <a href="#dashboards" data-toggle="collapse" class="sidebar-link">
                            <i class="align-middle mr-2 fas fa-fw fa-home"></i>
                            <span class="align-middle">Pelayanan</span>
                        </a>
                        <ul id="dashboards" class="sidebar-dropdown list-unstyled collapse show" data-parent="#sidebar">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="ktp_baru.php">Pembuatan KTP</a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="kk_baru.php">Pembuatan KK</a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="akte_lahir.php">Pembuatan Akta Kelahiran</a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="akte_kematian.php">Pembuatan Akta Kematian</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="surat_pindah.php" class="sidebar-link">Pembuatan Surat Pindah</a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a href="#selesai" data-toggle="collapse" class="sidebar-link">
                            <i class="align-middle mr-2 fas fa-fw fa-book"></i>
                            <span class="align-middle">Berkas Selesai</span>
                        </a>
                        <ul id="selesai" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="ktp_selesai.php">Pengajuan Cetak KTP</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="kk_selesai.php" class="sidebar-link">Pengajuan KK</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="akte_lahir_selesai.php" class="sidebar-link">Pengajuan Akte Kelahiran</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="akte_mati_selesai.php" class="sidebar-link">Pengajuan Akte Kematian</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="surat_pindah_selesai.php" class="sidebar-link">Pengajuan Surat Pindah</a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a href="#ditolak" data-toggle="collapse" class="sidebar-link">
                            <i class="align-middle mr-2 fas fa-fw fa-window-close"></i>
                            <span class="align-middle">Berkas Ditolak</span>
                        </a>
                        <ul id="ditolak" class="sidebar-dropdown list-unstyled collapse" data-parent="#sidebar">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="ktp_ditolak.php">Pengajuan Cetak KTP</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="kk_ditolak.php" class="sidebar-link">Pengajuan KK</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="akte_lahir_ditolak.php" class="sidebar-link">Pengajuan Akte Kelahiran</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="akte_mati_ditolak.php" class="sidebar-link">Pengajuan Akte Kematian</a>
                            </li>
                            <li class="sidebar-item">
                                <a href="surat_pindah_ditolak.php" class="sidebar-link">Pengajuan Surat Pindah</a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a href="file/pedoman.pdf" target="_blank" class="sidebar-link">
                            <i class="align-middle mr-2 fas fa-fw fa-download"></i>
                            <span class="align-middle">Pedoman Teknis</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>


        <div class="main">
            <nav class="navbar navbar-expand navbar-theme">
                <a href="#" class="sidebar-toggle d-flex mr-2">
                    <i class="hamburger align-self-center"></i>
                </a>

                <div class="navbar-collapse collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown ml-lg-2">
                            <a class="nav-link dropdown-toggle position-relative" href="#" id="userDropdown"
                                data-toggle="dropdown">
                                <i class="align-middle fas fa-cog"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                <a class="dropdown-item updateuser" href="#" id="<?= $row['id_user'] ?>">
                                    <i class="align-middle mr-1 fas fa-fw fa-user"></i>
                                    Profil</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalgantipassword">
                                    <i class="align-middle mr-1 fas fa-fw fa-cogs"></i> Ganti Password</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout.php">
                                    <i class="align-middle mr-1 fas fa-fw fa-arrow-alt-circle-right"></i>
                                    Log out
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="display_user"></div>

            <script>
            $(document).ready(function() {

                $(document).on('click', '.updateuser', function() {
                    var id = $(this).attr('id');

                    $("#display_user").html('');
                    $.ajax({
                        url: 'view_user.php',
                        type: 'POST',
                        cache: false,
                        data: {
                            id: id
                        },
                        success: function(data) {
                            $("#display_user").html(data);
                            $("#updateProfilModal").modal('show');
                        }
                    })

                })

            });
            </script>

            <!-- BEGIN modal gantifoto -->
            <div class="modal fade" id="modalgantifoto" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form action="scripts/function_user.php?act=gantifoto&id=<?= $row['id_user'] ?>" method="post"
                            enctype="multipart/form-data">
                            <div class="modal-header">
                                <h2 class="modal-title">Ganti Foto Profil</h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body m-3">
                                <div class="form-group row">
                                    <label class="form-label">Ganti Foto</label>
                                    <input type="file" class="form-control" name="foto">
                                    <input type="text" value="<?= $row['username'] ?>" name="username" hidden="true">
                                    <input type="text" value="<?= $row['id_user'] ?>" name="id_user" hidden="true">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-secondary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END modal gantifoto -->


            <!-- BEGIN  modal gantipassword -->
            <div class="modal fade" id="modalgantipassword" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form action="scripts/function_user.php?act=gantipassword&id=<?= $row['id_user'] ?>"
                            method="post">
                            <div class="modal-header">
                                <h2 class="modal-title">Ganti Password</h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body m-3">
                                <div class="form-group row">
                                    <label class="form-label">Password Baru</label>
                                    <input type="password" class="form-control" name="password">
                                    <input type="text" value="<?= $row['id_user'] ?>" name="id_user" hidden="true">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-secondary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END  modal ganti password -->
            <main class="content">
                <div class="container-fluid">
                    <div class="header">
                        <h1 class="header-title">Selamat Datang, <?php echo $row['nama_desa']; ?></h1>
                    </div>