<?php

include_once 'scripts/koneksi.php';

if (isset($_REQUEST['id'])) {
    $id_user = $_REQUEST['id'];

    $query = "SELECT * FROM tbl_users WHERE id_user = '$id_user'";
    $result = mysqli_query($connection, $query);
    $row = mysqli_fetch_assoc($result);

    $id_user = $row['id_user'];
    $username = $row['username'];
    $nama_pengguna = $row['nama_pengguna'];
    $no_hp = $row['no_hp'];
    $email = $row['email'];
    $nama_kades = $row['nama_kades'];
    $nama_desa = $row['nama_desa'];
    $alamat = $row['alamat'];
    $photo = $row['photo'];
}
?>

<!-- BEGIN  modal profil -->
<div class="modal fade" id="updateProfilModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Profil</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="scripts/function_user.php?act=updateprofile&id=<?= $id_user ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Username</label>
                        <input name="id_user" type="text" class="form-control" value="<?= $id_user ?>" hidden="true">
                        <input type="text" class="form-control" value="<?php echo $row['username'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No. Handphone</label>
                        <input id="no_hp" name="no_hp" type="number" class="form-control" value="<?= $no_hp ?>" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email</label>
                        <input id="email" name="email" type="email" class="form-control" value="<?= $email ?>" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Penanggung Jawab</label>
                        <input id="nama_pengguna" name="nama_pengguna" type="text" class="form-control" value="<?= $nama_pengguna ?>" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Kepala Desa</label>
                        <input id="nama_kades" name="nama_kades" type="text" class="form-control" value="<?= $nama_kades ?>" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Desa</label>
                        <input id="nama_desa" name="nama_desa" type="text" class="form-control" value="<?= $nama_desa ?>" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Alamat Kantor Desa</label>
                        <textarea id="alamat" name="alamat" class="form-control" rows="3" required><?= $alamat ?></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal profil -->