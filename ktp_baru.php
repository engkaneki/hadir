<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN CETAK KTP</h4>
                <p class="card-description">
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#pindah">Pengajaun Cetak
                        KTP</button>
                </p>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nomor Hp</td>
                                <td>Nama Pelapor</td>
                                <td>Tanggal Pengajuan</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $username = $row['username'];
                            $data = mysqli_query($connection, "select * from ktp where petugas='$username' AND status='pending' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {

                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $d['noreg']; ?></td>
                                    <td><?= $d['no_hp'] ?></td>
                                    <td><?= $d['nama'] ?></td>
                                    <td><?= format_tanggal_indonesia($d["tanggal_pengajuan"], 'tanggal_bulan_tahun'); ?>
                                    </td>
                                    <td>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#hapus<?= $no ?>">Hapus</button>

                                        <!-- modal delete -->
                                        <div class="modal fade" id="hapus<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                            hapus
                                                            pengajuan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 align="center">Menghapus Pengajuan atas nama
                                                            <?= $d['nama']; ?><strong><span class="grt"></span></strong> ?
                                                        </h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <a href="scripts/function_desa.php?act=deletektp&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal delete -->

                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<!-- BEGIN  modal buat surat pindah -->
<div class="modal fade" id="pindah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=ktp" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Ajukan Cetak KTP</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                        <input type="text" name="petugas" class="form-control" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Pelapor</label>
                        <input type="text" class="form-control" name="nik" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Pelapor</label>
                        <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Alamat Pelapor</label>
                        <textarea id="" cols="30" rows="10" class="form-control" name="alamat" required></textarea>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label for="" class="form-label">KK Pelapor</label>
                        <input type="file" class="form-control" name="kk">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-secondary">Simpan</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat akte kematian -->





<?php
include 'footer.php';
?>