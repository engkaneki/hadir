<?php
include 'header.php';
?>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Berikut data dan riwayat pengajuan masuk belum di proses</h4>
                        <p class="card-description">
                            <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#buatlaporan">Buat
                                Laporan</a>
                        </p>
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th hidden="true"></th>
                                        <th>No Registrasi</th>
                                        <th>NIK</th>
                                        <th>Nama Lengkap Pelapor</th>
                                        <th>Status Pelapor</th>
                                        <th>Nomor Telepon </th>
                                        <th>Jenis Laporan</th>
                                        <th>Proses</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php

                                include '../scripts/koneksi.php';

                                $no = 1;
                                $status = "pending";
                                $username = $_SESSION['username'];
                                $data=mysqli_query($connection, "select * from loket where status='$status' and petugas='$username' ORDER BY noreg DESC");
                                while ($d = mysqli_fetch_assoc($data)) {

                                ?>

                                    <tr>
                                        <td hidden="true"><?php echo $no++; ?></td>
                                        <td><?php echo $d['noreg']; ?></td>
                                        <td><?php echo $d['nik_pelapor'] ?></td>
                                        <td><?php echo $d['nama_pelapor'] ?></td>
                                        <td><?php echo $d['status_pelapor'] ?></td>
                                        <td><?php echo $d['nomor_telepon'] ?></td>
                                        <td><?php echo $d['laporan'] ?></td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-icon-text"
                                                data-bs-toggle="modal"
                                                data-bs-target="#deletepengajuan<?php echo $no; ?>">
                                                Hapus
                                            </button>

                                            <!-- modal delete -->
                                            <div class="modal fade" id="deletepengajuan<?php echo $no; ?>" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                                hapus
                                                                pengajuan</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 align="center">Menghapus Laporan atas nama
                                                                <?php echo $d['nama_pelapor'];?><strong><span
                                                                        class="grt"></span></strong> ?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Batal</button>
                                                            <a href="../scripts/function_user.php?act=deletelaporan&id=<?php echo $d['id']; ?>"
                                                                class="btn btn-danger">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- modal delete -->



                                        </td>
                                    </tr>
                                    <?php
                                }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row end -->

        <?php
        $tgl_hari_ini = date("Y-m-d");
        $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM loket WHERE tgl_pengajuan='$tgl_hari_ini'");
        $data = mysqli_fetch_array($q);
        $noregis = $data['noTerbesar'];
        $urutan = (int) substr($noregis, 20, 3);
        $urutan++;
        $kode = "1219";
        $huruf = "loket";
        $tgl = date("dmY");
        $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);
        ?>


        <!-- Modal Buat Laporan -->
        <div class="modal fade" id="buatlaporan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <form action="../scripts/function_user.php?act=tambahlaporan" class="modal-content" method="POST"
                    enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pengajuan Laporan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3>
                            Pastikan sudah membawa semua dokumen persyaratan
                        </h3>

                        <div>
                            <hr class="dropdown divider">
                        </div>

                        <div class="mb-3">
                            <input type="text" class="form-control" name="noregis" value="<?php echo $noregis; ?>"
                                readonly>
                        </div>

                        <br>
                        pastikan nomor telepon aktif dan bisa dihubungi

                        <div class="mb-3">
                            <label class="form-label">Nomor Telepon*</label>
                            <input type="number" class="form-control" name="nomor_telepon" placeholder="Nomor Telepon*">
                        </div>
                        pastikan isi dengan alamat email yang benar
                        <div class="mb-3">
                            <label class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email*">
                        </div>

                        <div class="mb-3">
                            <label class="form-label">NIK Pelapor*</label>
                            <input type="number" class="form-control" name="nik_pelapor" required>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Nama Pelapor*</label>
                            <input type="text" class="form-control" name="nama_pelapor" required>
                        </div>

                        <div class="mb-3" hidden="true">
                            <input type="text" class="form-control" name="petugas" required
                                value="<?php echo $_SESSION['username'] ?>">
                        </div>


                        <div class="mb-3" hidden="true">
                            <input type="date" class="form-control" name="tgl_pengajuan" required
                                value="<?php echo date("Y-m-d"); ?>">
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Status Pelapor</label>
                            <select class="form-select" name="status_pelapor">
                                <option value=""></option>
                                <option value="Pengurusan Dokumen Pribadi">Pengurusan Dokumen Pribadi</option>
                                <option value="Pengurusan Dokumen Orang Lain">Pengurusan Dokumen Orang Lain</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Pilihan Pengurusan Dokumen</label>
                        </div>

                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Biodata Penduduk">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Biodata Penduduk (Bagi masyarakat yang belum pernah memiliki administrasi
                                    kependudukan)
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Kartu Keluarga">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Kartu Keluarga
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" KTP Elektronik">
                                <label class="form-check-label" for="flexCheckChecked">
                                    KTP Elektronik
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]" value=" KIA">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Kartu Identitas Anak (KIA)
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Surat Keterangan Pindah">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Surat Keterangan Pindah
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Akta Kelahiran">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Akta Kelahiran
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Akta Perkawinan">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Akta Perkawinan
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Akta Perceraian">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Akta Perceraian
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]" value=" Akta Kematian">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Akta Kematian
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Perubahan Status Anak">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Perubahan Status Anak
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]"
                                    value=" Pembetulan & Pembatalan Akta">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Pembetulan & Pembatalan Akta
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="laporan[]" value=" SKTT">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Surat Keterangan Tempat Tinggal (SKTT)
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" name="simpan" class="btn btn-success">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Modal Buat Laporan -->

        <?php
        include '../footer.php';
        ?>