<?php
include 'header.php';
?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN AKTE KELAHIRAN BARU YANG SUDAH SELESAI</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nomor Registrasi</td>
                                <td>Nama Pelapor</td>
                                <td>Nama Anak</td>
                                <td>Nomor Hp</td>
                                <td>Tanggal Pengajuan</td>
                                <td>Desa</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            include 'scripts/koneksi.php';
                            $no = 1;
                            $data = mysqli_query($connection, "select * from akte_baru_lahir where status='selesai' order by id desc");
                            while ($d = mysqli_fetch_assoc($data)) {
                                $petugas = $d['petugas'];
                                $q = mysqli_query($connection, "SELECT * FROM tbl_users WHERE username='$petugas'");
                                while ($t = mysqli_fetch_assoc($q)) {
                                    $nama_desa = $t['nama_desa'];
                                    $email = $t['email'];
                                    $no_hp = $t['no_hp'];

                            ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d['noreg']; ?></td>
                                        <td><?= $d['nama_ayah'] ?></td>
                                        <td><?= $d['nama_anak'] ?></td>
                                        <td><?= $d['no_hp'] ?></td>
                                        <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                        <td><?= $nama_desa ?></td>
                                        <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>


                                            <!-- BEGIN  modal detail -->
                                            <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <form action="scripts/function_desa.php?act=kkbaru" method="POST" enctype="multipart/form-data">

                                                            <?php
                                                            $id = $d['id'];
                                                            $query2 = "SELECT * FROM akte_baru_lahir WHERE id='$id'";
                                                            $result = mysqli_query($connection, $query2);
                                                            while ($row2 = mysqli_fetch_assoc($result)) {

                                                            ?>


                                                                <div class="modal-header">
                                                                    <h2 class="modal-title">Detail Akte Kelahiran <?= $row2['nama_anak'] ?></h2>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body m-3">
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Tanggal Pengajuan</label>
                                                                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Email Desa</label>
                                                                        <input type="text" class="form-control" value="<?= $email ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">No HP Desa</label>
                                                                        <input type="number" class="form-control" value="<?= $no_hp ?>" readonly>
                                                                    </div>
                                                                    <div>
                                                                        <hr class="dropdown-divider">
                                                                    </div>
                                                                    <h3>DATA PELAPOR</h3>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">No HP</label>
                                                                        <input type="number" class="form-control" value="<?= $row2['no_hp'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">No Kartu Keluarga</label>
                                                                        <input type="number" class="form-control" value="<?= $row2['no_kk'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">NIK Ayah</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nik_ayah'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Nama Ayah</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nama_ayah'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">NIK Ibu</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nik_ibu'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Nama Ibu</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nama_ibu'] ?>" readonly>
                                                                    </div>
                                                                    <div>
                                                                        <hr class="dropdown-divider">
                                                                    </div>
                                                                    <h3>DATA ANAK</h3>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Nama Lengkap Anak</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['nama_anak'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Jenis Kelamin</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['jk_anak'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Tanggal Lahir</label>
                                                                        <input type="date" class="form-control" value="<?= $row2['tgl_lahir'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Jenis Kelahiran</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['jenis_kelahiran'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Anak Ke</label>
                                                                        <input type="number" class="form-control" value="<?= $row2['anak_ke'] ?>" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Berat Anak</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['berat'] ?> gr" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Panjang Anak</label>
                                                                        <input type="text" class="form-control" value="<?= $row2['panjang'] ?> cm" readonly>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="form-label">Detail Berkas</label>
                                                                        <textarea class="form-control" name="" id="" cols="30" rows="10" readonly><?= $row2['detail'] ?></textarea>
                                                                    </div>
                                                                    <div>
                                                                        <hr class="dropdown-divider">
                                                                    </div>
                                                                    <h3>BERKAS PERSYARATAN</h3>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['surat_pengantar']; ?>">Surat Pengantar</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['surat_lahir']; ?>">Surat Kelahiran</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['kk']; ?>">KK Orang Tua</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['ktp_ayah']; ?>">KTP Ayah</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['ktp_ibu']; ?>">KTP Ibu</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['buku_nikah']; ?>">Buku Nikah</a>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <a class="btn btn-info" target="_blank" href="../file desa/akte lahir/<?php echo $row2['lainnya']; ?>">Berkas Lainnya</a>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END  modal detail -->


                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<?php
include 'footer.php';
?>