<?php
include 'header.php';
$username = $row['username'];

$kkbr = "SELECT * FROM kk WHERE petugas='$username' AND status='pending'";
$databr = mysqli_query($connection, $kkbr);
$jumlah_kkbr = mysqli_num_rows($databr);

$kkrs = "SELECT * FROM kkrusak WHERE petugas='$username' AND status='pending'";
$datars = mysqli_query($connection, $kkrs);
$jumlah_kkrusak = mysqli_num_rows($datars);

$kkub = "SELECT * FROM kkubah WHERE petugas='$username' AND status='pending'";
$dataub = mysqli_query($connection, $kkub);
$jumlah_kkubah = mysqli_num_rows($dataub);

$jumlah_kk = $jumlah_kkbr + $jumlah_kkrusak + $jumlah_kkubah;

$akte_lahir = "SELECT * FROM akte_baru_lahir WHERE petugas='$username' AND status='pending'";
$data2 = mysqli_query($connection, $akte_lahir);
$jumlah_akte_lahir = mysqli_num_rows($data2);

$akte_mati = "SELECT * FROM akte_mati WHERE petugas='$username' AND status='pending'";
$data3 = mysqli_query($connection, $akte_mati);
$jumlah_akte_mati = mysqli_num_rows($data3);

$surat_pindah = "SELECT * FROM surat_pindah WHERE petugas='$username' AND status='pending'";
$data4 = mysqli_query($connection, $surat_pindah);
$jumlah_surat_pindah = mysqli_num_rows($data4);


?>

<!-- Main Content-->
<div class="row">
    <div class="col-lg-12 grid-margin strect-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">DATA PENGAJUAN KARTU KELUARGA BARU</h4>
                <p class="card-description">
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Buat KK <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu">
                        <a data-toggle="modal" data-target="#buatkk" href="#" class="dropdown-item">KK Keluarga Baru</a>
                        <a data-toggle="modal" data-target="#kkrusak" href="#" class="dropdown-item">KK Hilang/Rusak</a>
                        <a data-toggle="modal" data-target="#kkubah" href="#" class="dropdown-item">KK Perubahan Data</a>
                    </div>
                </div>
                <!-- <button class="btn btn-secondary" data-toggle="modal" data-target="#buatkk">Buat KK</button> -->
                </p>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">
                            KK Baru
                            <span class="badge badge-danger"><?= $jumlah_kkbr ?></span>
                        </a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">
                            KK Hilang/Rusak
                            <span class="badge badge-danger"><?= $jumlah_kkrusak ?></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">
                            KK Perubahan Data
                            <span class="badge badge-danger"><?= $jumlah_kkubah ?></span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <!-- KK Baru -->
                    <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered" id="myTable">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Nomor Registrasi</td>
                                        <td>Nama Pelapor</td>
                                        <td>Nomor Hp</td>
                                        <td>Tanggal Pengajuan</td>
                                        <td>Ket</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    include 'scripts/koneksi.php';
                                    $no = 1;
                                    $data = mysqli_query($connection, "select * from kk where petugas='$username' AND status='pending' order by id desc");
                                    while ($d = mysqli_fetch_assoc($data)) {

                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $d['noreg']; ?></td>
                                            <td><?= $d['nama_suami'] ?></td>
                                            <td><?= $d['no_hp'] ?></td>
                                            <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                            <td><?= $d['detail'] ?></td>
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal" data-target="#detail<?= $no ?>">Detail</button>
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#hapus<?= $no ?>">Hapus</button>

                                                <!-- BEGIN  modal detail -->
                                                <div class="modal fade" id="detail<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <form action="scripts/function_desa.php?act=kkbaru" method="POST" enctype="multipart/form-data">

                                                                <?php
                                                                $id = $d['id'];
                                                                $query2 = "SELECT * FROM kk WHERE id='$id'";
                                                                $result = mysqli_query($connection, $query2);
                                                                while ($row2 = mysqli_fetch_assoc($result)) {
                                                                ?>

                                                                    <div class="modal-header">
                                                                        <h2 class="modal-title">Detail Kartu Keluarga <?= $row2['nama_suami'] ?></h2>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body m-3">
                                                                        <div class="form-group row">
                                                                            <input class="form-control" name="id" type="text" value="<?= $row2['id'] ?>" hidden="true">
                                                                            <label class="form-label">Tanggal Pengajuan</label>
                                                                            <input type="date" class="form-control" name="tgl_pengajuan" value="<?= date('Y-m-d', strtotime($row2['tgl_pengajuan'])) ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Email Desa</label>
                                                                            <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP Desa</label>
                                                                            <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>DATA PELAPOR</h3>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP</label>
                                                                            <input type="number" class="form-control" name="no_hp" value="<?= $row2['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">NIK Suami</label>
                                                                            <input type="text" class="form-control" name="nik_suami" value="<?= $row2['nik_suami'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Nama Suami</label>
                                                                            <input type="text" class="form-control" name="nama_suami" value="<?= $row2['nama_suami'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">NIK Istri</label>
                                                                            <input type="text" class="form-control" name="nik_istri" value="<?= $row2['nik_istri'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Nama Istri</label>
                                                                            <input type="text" class="form-control" name="nama_istri" value="<?= $row2['nama_istri'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Detail Pengajuan Berkas</label>
                                                                            <textarea class="form-control" name="detail" id="" cols="30" rows="10" readonly><?= $row2['detail'] ?></textarea>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>BERKAS PERSYARATAN</h3>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['kk_suami']; ?>">Kartu Keluarga Orang Tua Suami</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['kk_istri']; ?>">Kartu Keluraga Orang Tua Istri</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['ktp_suami']; ?>">KTP Suami</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['ktp_istri']; ?>">KTP Istri</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['buku_nikah']; ?>">Buku Nikah</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['buku_nikah_ortu']; ?>">Buku Nikah Orang Tua Suami/SPTJM</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['buku_nikah_ortu2']; ?>">Buku Nikah Orang Tua Istri/SPTJM</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['surat_pindah']; ?>">Surat Keterangan Pindah</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/<?php echo $row2['lainnya'] ?>">Berkas Lainnya</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- modal delete -->
                                                <div class="modal fade" id="hapus<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                                    hapus
                                                                    pengajuan</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4 align="center">Menghapus Pengajuan atas nama <?= $d['nama_suami']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                <a href="scripts/function_desa.php?act=delete&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal delete -->

                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- KK Hilang/Rusak -->
                    <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered" id="myTable2">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Nomor Registrasi</td>
                                        <td>Nama Pelapor</td>
                                        <td>Nomor Hp</td>
                                        <td>Tanggal Pengajuan</td>
                                        <td>Ket</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    include 'scripts/koneksi.php';
                                    $no = 1;
                                    $username = $row['username'];
                                    $data = mysqli_query($connection, "select * from kkrusak where petugas='$username' AND status='pending' order by id desc");
                                    while ($d = mysqli_fetch_assoc($data)) {

                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $d['noreg']; ?></td>
                                            <td><?= $d['nama'] ?></td>
                                            <td><?= $d['no_hp'] ?></td>
                                            <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                            <td><?= $d['detail'] ?></td>
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal" data-target="#detail2<?= $no ?>">Detail</button>
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#hapus2<?= $no ?>">Hapus</button>

                                                <!-- BEGIN  modal detail -->
                                                <div class="modal fade" id="detail2<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <form>

                                                                <?php
                                                                $id = $d['id'];
                                                                $query2 = "SELECT * FROM kkrusak WHERE id='$id'";
                                                                $result = mysqli_query($connection, $query2);
                                                                while ($row2 = mysqli_fetch_assoc($result)) {
                                                                ?>

                                                                    <div class="modal-header">
                                                                        <h2 class="modal-title">Detail Kartu Keluarga <?= $row2['nama'] ?></h2>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body m-3">
                                                                        <div class="form-group row">
                                                                            <input class="form-control" name="id" type="text" value="<?= $row2['id'] ?>" hidden="true">
                                                                            <label class="form-label">Tanggal Pengajuan</label>
                                                                            <input type="date" class="form-control" name="tgl_pengajuan" value="<?= date('Y-m-d', strtotime($row2['tgl_pengajuan'])) ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Email Desa</label>
                                                                            <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP Desa</label>
                                                                            <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>DATA PELAPOR</h3>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP</label>
                                                                            <input type="number" class="form-control" name="no_hp" value="<?= $row2['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">NIK</label>
                                                                            <input type="text" class="form-control" name="nik" value="<?= $row2['nik'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Nama</label>
                                                                            <input type="text" class="form-control" name="nama" value="<?= $row2['nama'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Detail Pengajuan Berkas</label>
                                                                            <textarea class="form-control" name="detail" id="" cols="30" rows="10" readonly><?= $row2['detail'] ?></textarea>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>BERKAS PERSYARATAN</h3>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkrusak/<?php echo $row2['ktp']; ?>">KTP</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkrusak/<?php echo $row2['surat_hilang']; ?>">Surat Keterangan Hilang</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkrusak/<?php echo $row2['kk']; ?>">KK</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkrusak/<?php echo $row2['lainnya'] ?>">Berkas Lainnya</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- modal delete -->
                                                <div class="modal fade" id="hapus2<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                                    hapus
                                                                    pengajuan</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4 align="center">Menghapus Pengajuan atas nama <?= $d['nama']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                <a href="scripts/function_desa.php?act=deletekkrusak&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal delete -->

                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- KK Perubahan Data -->
                    <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered" id="myTable3">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Nomor Registrasi</td>
                                        <td>Nama Pelapor</td>
                                        <td>Nomor Hp</td>
                                        <td>Tanggal Pengajuan</td>
                                        <td>Ket</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    include 'scripts/koneksi.php';
                                    $no = 1;
                                    $username = $row['username'];
                                    $data = mysqli_query($connection, "select * from kkubah where petugas='$username' AND status='pending' order by id desc");
                                    while ($d = mysqli_fetch_assoc($data)) {

                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $d['noreg']; ?></td>
                                            <td><?= $d['nama'] ?></td>
                                            <td><?= $d['no_hp'] ?></td>
                                            <td><?= format_tanggal_indonesia($d["tgl_pengajuan"], 'tanggal_bulan_tahun'); ?></td>
                                            <td><?= $d['detail'] ?></td>
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal" data-target="#detail3<?= $no ?>">Detail</button>
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#hapus3<?= $no ?>">Hapus</button>

                                                <!-- BEGIN  modal detail -->
                                                <div class="modal fade" id="detail3<?= $no ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <form>

                                                                <?php
                                                                $id = $d['id'];
                                                                $query2 = "SELECT * FROM kkubah WHERE id='$id'";
                                                                $result = mysqli_query($connection, $query2);
                                                                while ($row2 = mysqli_fetch_assoc($result)) {
                                                                ?>

                                                                    <div class="modal-header">
                                                                        <h2 class="modal-title">Detail Kartu Keluarga <?= $row2['nama'] ?></h2>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body m-3">
                                                                        <div class="form-group row">
                                                                            <input class="form-control" name="id" type="text" value="<?= $row2['id'] ?>" hidden="true">
                                                                            <label class="form-label">Tanggal Pengajuan</label>
                                                                            <input type="date" class="form-control" name="tgl_pengajuan" value="<?= date('Y-m-d', strtotime($row2['tgl_pengajuan'])) ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Email Desa</label>
                                                                            <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP Desa</label>
                                                                            <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>DATA PELAPOR</h3>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">No HP</label>
                                                                            <input type="number" class="form-control" name="no_hp" value="<?= $row2['no_hp'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">NIK</label>
                                                                            <input type="text" class="form-control" name="nik" value="<?= $row2['nik'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Nama</label>
                                                                            <input type="text" class="form-control" name="nama" value="<?= $row2['nama'] ?>" readonly>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="form-label">Detail Pengajuan Berkas</label>
                                                                            <textarea class="form-control" name="detail" id="" cols="30" rows="10" readonly><?= $row2['detail'] ?></textarea>
                                                                        </div>
                                                                        <div>
                                                                            <hr class="dropdown-divider">
                                                                        </div>
                                                                        <h3>BERKAS PERSYARATAN</h3>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkubah/<?php echo $row2['ktp']; ?>">KTP</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkubah/<?php echo $row2['surat_keterangan']; ?>">Surat Keterangan Bukti Perubahan</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkubah/<?php echo $row2['kk']; ?>">KK</a>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <a class="btn btn-info" target="_blank" href="file desa/kk/kkubah/<?php echo $row2['lainnya'] ?>">Berkas Lainnya</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- modal delete -->
                                                <div class="modal fade" id="hapus3<?= $no; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi
                                                                    hapus
                                                                    pengajuan</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4 align="center">Menghapus Pengajuan atas nama <?= $d['nama']; ?><strong><span class="grt"></span></strong> ?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                <a href="scripts/function_desa.php?act=deletekkubah&id=<?= $d['id']; ?>" class="btn btn-danger">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal delete -->

                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
</div>
</main>
<!-- End of Main Content-->


<!-- BEGIN  modal buat kartu keluarga -->
<div class="modal fade" id="buatkk" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=kkbaru" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Buat Kartu Keluarga Baru</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Suami</label>
                        <input type="text" class="form-control" name="nik_suami" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Suami</label>
                        <input type="text" class="form-control" name="nama_suami" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Istri</label>
                        <input type="text" class="form-control" name="nik_istri" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Istri</label>
                        <input type="text" class="form-control" name="nama_istri" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Detail Pengajuan Berkas</label>
                        <textarea class="form-control" name="detail" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label class="form-label">Kartu Keluarga Orang Tua Suami</label>
                        <input type="file" class="form-control" name="kk_suami">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Kartu Keluraga Orang Tua Istri</label>
                        <input type="file" class="form-control" name="kk_istri">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KTP Suami</label>
                        <input type="file" class="form-control" name="ktp_suami">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KTP Istri</label>
                        <input type="file" class="form-control" name="ktp_istri">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Buku Nikah</label>
                        <input type="file" class="form-control" name="buku_nikah">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Buku Nikah Orang Tua Suami / SPTJM</label>
                        <input type="file" class="form-control" name="buku_nikah_ortu">
                        <input type="text" name="petugas" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Buku Nikah Orang Tua Istri / SPTJM</label>
                        <input type="file" class="form-control" name="buku_nikah_ortu2">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Keterangan Pindah (Jika penduduk berasal dari luar daerah)</label>
                        <input type="file" class="form-control" name="surat_pindah">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Berkas Lainnya</label>
                        <input type="file" class="form-control" name="lainnya">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat kartu keluarga -->

<!-- BEGIN  modal buat kartu keluarga Hilang/Rusak -->
<div class="modal fade" id="kkrusak" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=kkrusak" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Buat Kartu Keluarga Baru Karena Hilang/Rusak</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                        <input type="text" name="petugas" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Pelapor</label>
                        <input type="text" class="form-control" name="nik" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Pelapor</label>
                        <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Detail Pengajuan Berkas</label>
                        <textarea class="form-control" name="detail" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label class="form-label">KTP</label>
                        <input type="file" class="form-control" name="ktp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Keterangan Hilang dari Kepolisian (isi jika KK hilang)</label>
                        <input type="file" class="form-control" name="surat_hilang">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KK Yang Rusak (isi jika KK Rusak)</label>
                        <input type="file" class="form-control" name="kk">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Berkas Lainnya</label>
                        <input type="file" class="form-control" name="lainnya">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat kartu keluarga -->


<!-- BEGIN  modal buat kartu keluarga Perubahan Data -->
<div class="modal fade" id="kkubah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="scripts/function_desa.php?act=kkubah" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h2 class="modal-title">Buat Kartu Keluarga Baru Karena Perubahan Data</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="form-group row">
                        <label class="form-label">Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tgl_pengajuan" value="<?php echo date('Y-m-d', strtotime($tgl_hari_ini)) ?>" readonly>
                        <input type="text" name="petugas" value="<?= $row['username'] ?>" hidden="true">
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Email Desa</label>
                        <input type="text" class="form-control" value="<?= $row['email'] ?>" readonly>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">No HP Desa</label>
                        <input type="number" class="form-control" value="<?= $row['no_hp'] ?>" readonly>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>DATA PELAPOR</h3>
                    <div class="form-group row">
                        <label class="form-label">No HP</label>
                        <input type="number" class="form-control" name="no_hp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">NIK Pelapor</label>
                        <input type="text" class="form-control" name="nik" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Nama Pelapor</label>
                        <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Detail Pengajuan Berkas</label>
                        <textarea class="form-control" name="detail" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div>
                        <hr class="dropdown-divider">
                    </div>
                    <h3>BERKAS PERSYARATAN</h3>
                    <div class="form-group row">
                        <label class="form-label">KTP</label>
                        <input type="file" class="form-control" name="ktp" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Surat Keterangan / Bukti Perubahan Data</label>
                        <input type="file" class="form-control" name="surat_keterangan" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">KK</label>
                        <input type="file" class="form-control" name="kk" required>
                    </div>
                    <div class="form-group row">
                        <label class="form-label">Berkas Lainnya</label>
                        <input type="file" class="form-control" name="lainnya">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-secondary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END  modal buat kartu keluarga -->





<?php
include 'footer.php';
?>