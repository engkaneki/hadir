<?php

include 'koneksi.php';

if ($_GET['act'] == 'kkbaru') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM kk WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 24, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/kk";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $no_hp = $_POST['no_hp'];
    $nik_suami = $_POST['nik_suami'];
    $nama_suami = $_POST['nama_suami'];
    $nik_istri = $_POST['nik_istri'];
    $nama_istri =  $_POST['nama_istri'];
    $tgl_pengajuan = $_POST['tgl_pengajuan'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";
    $detail = $_POST['detail'];

    //files
    $kk_suami_name = $_FILES['kk_suami']['name'];
    $kk_suami_tmp = $_FILES['kk_suami']['tmp_name'];
    $kk_istri_name = $_FILES['kk_istri']['name'];
    $kk_istri_tmp = $_FILES['kk_istri']['tmp_name'];
    $ktp_suami_name = $_FILES['ktp_suami']['name'];
    $ktp_suami_tmp = $_FILES['ktp_suami']['tmp_name'];
    $ktp_istri_name = $_FILES['ktp_istri']['name'];
    $ktp_istri_tmp = $_FILES['ktp_istri']['tmp_name'];
    $buku_nikah_name = $_FILES['buku_nikah']['name'];
    $buku_nikah_tmp = $_FILES['buku_nikah']['tmp_name'];
    $buku_nikah_ortu_name = $_FILES['buku_nikah_ortu']['name'];
    $buku_nikah_ortu_tmp = $_FILES['buku_nikah_ortu']['tmp_name'];
    $buku_nikah_ortu2_name = $_FILES['buku_nikah_ortu2']['name'];
    $buku_nikah_ortu2_tmp = $_FILES['buku_nikah_ortu2']['tmp_name'];
    $surat_pindah_name = $_FILES['surat_pindah']['name'];
    $surat_pindah_tmp = $_FILES['surat_pindah']['tmp_name'];
    $lainnya_name = $_FILES['lainnya']['name'];
    $lainnya_tmp = $_FILES['lainnya']['tmp_name'];

    $kk_suami = $tgl_pengajuan . "_" . $nama_suami . "_" . "kk suami" . "_" . $kk_suami_name;
    $kk_istri = $tgl_pengajuan . "_" . $nama_suami . "_" . "kk istri" . "_" . $kk_istri_name;
    $ktp_suami = $tgl_pengajuan . "_" . $nama_suami . "_" . "ktp suami" . "_" . $ktp_suami_name;
    $ktp_istri = $tgl_pengajuan . "_" . $nama_suami . "_" . "ktp istri" . "_" . $ktp_istri_name;
    $buku_nikah = $tgl_pengajuan . "_" . $nama_suami . "_" . "buku nikah" . "_" . $buku_nikah_name;
    $buku_nikah_ortu = $tgl_pengajuan . "_" . $nama_suami . "_" . "buku nikah ortu" . "_" . $buku_nikah_ortu_name;
    $buku_nikah_ortu2 = $tgl_pengajuan . "_" . $nama_suami . "_" . "buku nikah ortu2" . "_" . $buku_nikah_ortu2_name;
    $surat_pindah = $tgl_pengajuan . "_" . $nama_suami . "_" . "Surat Pindah" . "_" . $surat_pindah_name;
    $lainnya = $tgl_pengajuan . "-" . $nama_suami . "_" . "Berkas Lainnya" . "_" . $lainnya_name;
    move_uploaded_file($kk_suami_tmp, '../file desa/kk/' . $kk_suami);
    move_uploaded_file($kk_istri_tmp, '../file desa/kk/' . $kk_istri);
    move_uploaded_file($ktp_suami_tmp, '../file desa/kk/' . $ktp_suami);
    move_uploaded_file($ktp_istri_tmp, '../file desa/kk/' . $ktp_istri);
    move_uploaded_file($buku_nikah_tmp, '../file desa/kk/' . $buku_nikah);
    move_uploaded_file($buku_nikah_ortu_tmp, '../file desa/kk/' . $buku_nikah_ortu);
    move_uploaded_file($buku_nikah_ortu2_tmp, '../file desa/kk/' . $buku_nikah_ortu2);
    move_uploaded_file($surat_pindah_tmp, '../file desa/kk/' . $surat_pindah);
    move_uploaded_file($lainnya_tmp, '../file desa/kk/' . $lainnya);

    $simpankk = "INSERT INTO kk(noreg, no_hp, nik_suami, nama_suami, nik_istri, nama_istri, tgl_pengajuan, kk_suami, kk_istri, ktp_suami, ktp_istri, buku_nikah, buku_nikah_ortu, buku_nikah_ortu2, surat_pindah, lainnya, status, petugas, ket, detail) VALUES ('$noregis', '$no_hp', '$nik_suami', '$nama_suami', '$nik_istri', '$nama_istri', '$tgl_pengajuan', '$kk_suami', '$kk_istri', '$ktp_suami', '$ktp_istri', '$buku_nikah', '$buku_nikah_ortu', '$buku_nikah_ortu2', '$surat_pindah', '$lainnya', '$status', '$petugas', '$ket', '$detail')";
    if ($connection->query($simpankk)) {
        header("location:../kk_baru.php");
    } else {
        header("location:../kk_baru.php");
    }
} else if ($_GET['act'] == 'kkrusak') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM kkrusak WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 29, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/kkrusak";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $no_hp = $_POST['no_hp'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $tgl_pengajuan = $_POST['tgl_pengajuan'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";
    $detail = $_POST['detail'];

    //files
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];
    $ktp_name = $_FILES['ktp']['name'];
    $ktp_tmp = $_FILES['ktp']['tmp_name'];
    $surat_hilang_name = $_FILES['surat_hilang']['name'];
    $surat_hilang_tmp = $_FILES['surat_hilang']['tmp_name'];
    $lainnya_name = $_FILES['lainnya']['name'];
    $lainnya_tmp = $_FILES['lainnya']['tmp_name'];

    $kk = $tgl_pengajuan . "_" . $nama . "_" . "kk" . "_" . $kk_name;
    $ktp = $tgl_pengajuan . "_" . $nama . "_" . "ktp" . "_" . $ktp_name;
    $surat_hilang = $tgl_pengajuan . "_" . $nama . "_" . "Surat Hilang" . "_" . $surat_hilang_name;
    $lainnya = $tgl_pengajuan . "-" . $nama . "_" . "Berkas Lainnya" . "_" . $lainnya_name;
    move_uploaded_file($kk_tmp, '../file desa/kk/kkrusak/' . $kk);
    move_uploaded_file($ktp_tmp, '../file desa/kk/kkrusak/' . $ktp);
    move_uploaded_file($surat_hilang_tmp, '../file desa/kk/kkrusak/' . $surat_hilang);
    move_uploaded_file($lainnya_tmp, '../file desa/kk/kkrusak/' . $lainnya);

    $simpankk = "INSERT INTO kkrusak(noreg, no_hp, nik, nama, tgl_pengajuan, kk, ktp, surat_hilang, lainnya, status, petugas, ket, detail) VALUES ('$noregis', '$no_hp', '$nik', '$nama', '$tgl_pengajuan', '$kk', '$ktp', '$surat_hilang', '$lainnya', '$status', '$petugas', '$ket', '$detail')";
    if ($connection->query($simpankk)) {
        header("location:../kk_baru.php");
    } else {
        header("location:../kk_baru.php");
    }
} else if ($_GET['act'] == 'kkubah') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM kkubah WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 28, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/kkubah";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $no_hp = $_POST['no_hp'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $tgl_pengajuan = $_POST['tgl_pengajuan'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";
    $detail = $_POST['detail'];

    //files
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];
    $ktp_name = $_FILES['ktp']['name'];
    $ktp_tmp = $_FILES['ktp']['tmp_name'];
    $surat_keterangan_name = $_FILES['surat_keterangan']['name'];
    $surat_keterangan_tmp = $_FILES['surat_keterangan']['tmp_name'];
    $lainnya_name = $_FILES['lainnya']['name'];
    $lainnya_tmp = $_FILES['lainnya']['tmp_name'];

    $kk = $tgl_pengajuan . "_" . $nama . "_" . "kk" . "_" . $kk_name;
    $ktp = $tgl_pengajuan . "_" . $nama . "_" . "ktp" . "_" . $ktp_name;
    $surat_keterangan = $tgl_pengajuan . "_" . $nama . "_" . "Surat Keterangan" . "_" . $surat_keterangan_name;
    $lainnya = $tgl_pengajuan . "-" . $nama . "_" . "Berkas Lainnya" . "_" . $lainnya_name;
    move_uploaded_file($kk_tmp, '../file desa/kk/kkubah/' . $kk);
    move_uploaded_file($ktp_tmp, '../file desa/kk/kkubah/' . $ktp);
    move_uploaded_file($surat_keterangan_tmp, '../file desa/kk/kkubah/' . $surat_keterangan);
    move_uploaded_file($lainnya_tmp, '../file desa/kk/kkubah/' . $lainnya);

    $simpankk = "INSERT INTO kkubah(noreg, no_hp, nik, nama, tgl_pengajuan, kk, ktp, surat_keterangan, lainnya, status, petugas, ket, detail) VALUES ('$noregis', '$no_hp', '$nik', '$nama', '$tgl_pengajuan', '$kk', '$ktp', '$surat_keterangan', '$lainnya', '$status', '$petugas', '$ket', '$detail')";
    if ($connection->query($simpankk)) {
        header("location:../kk_baru.php");
    } else {
        header("location:../kk_baru.php");
    }
} else if ($_GET['act'] == 'delete') {
    $id = $_GET['id'];
    $get_file = "SELECT kk_suami, kk_istri, ktp_suami, ktp_istri, buku_nikah, buku_nikah_ortu, buku_nikah_ortu2, surat_pindah, lainnya FROM kk WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/kk/" . $file_old['kk_suami']);
    unlink("../file desa/kk/" . $file_old['kk_istri']);
    unlink("../file desa/kk/" . $file_old['ktp_suami']);
    unlink("../file desa/kk/" . $file_old['ktp_istri']);
    unlink("../file desa/kk/" . $file_old['buku_nikah']);
    unlink("../file desa/kk/" . $file_old['buku_nikah_ortu']);
    unlink("../file desa/kk/" . $file_old['buku_nikah_ortu2']);
    unlink("../file desa/kk/" . $file_old['surat_pindah']);
    unlink("../file desa/kk" . $file_old['lainnya']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM kk WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../kk_baru.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'deletekkrusak') {
    $id = $_GET['id'];
    $get_file = "SELECT kk, ktp, surat_hilang, lainnya FROM kkrusak WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/kk/kkrusak/" . $file_old['kk']);
    unlink("../file desa/kk/kkrusak/" . $file_old['ktp']);
    unlink("../file desa/kk/kkrusak/" . $file_old['surat_hilang']);
    unlink("../file desa/kk/kkrusak/" . $file_old['lainnya']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM kkrusak WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../kk_baru.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'deletekkubah') {
    $id = $_GET['id'];
    $get_file = "SELECT kk, ktp, surat_keterangan, lainnya FROM kkubah WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/kk/kkubah/" . $file_old['kk']);
    unlink("../file desa/kk/kkubah/" . $file_old['ktp']);
    unlink("../file desa/kk/kkubah/" . $file_old['surat_keterangan']);
    unlink("../file desa/kk/kkubah/" . $file_old['lainnya']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM kkubah WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../kk_baru.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'aktebaru') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM akte_baru_lahir WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 24, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/al";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $no_hp = $_POST['no_hp'];
    $no_kk = $_POST['no_kk'];
    $nama_anak = $_POST['nama_anak'];
    $jk_anak = $_POST['jk_anak'];
    $tgl_lahir = $_POST['tgl_lahir'];
    $jenis_kelahiran = $_POST['jenis_kelahiran'];
    $anak_ke = $_POST['anak_ke'];
    $tgl_pengajuan = $_POST['tgl_pengajuan'];
    $berat = $_POST['berat'];
    $panjang = $_POST['panjang'];
    $nik_ibu = $_POST['nik_ibu'];
    $nama_ibu = $_POST['nama_ibu'];
    $nik_ayah = $_POST['nik_ayah'];
    $nama_ayah = $_POST['nama_ayah'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";
    $detail = $_POST['detail'];

    //files
    $surat_pengantar_name = $_FILES['surat_pengantar']['name'];
    $surat_pengantar_tmp = $_FILES['surat_pengantar']['tmp_name'];
    $surat_lahir_name = $_FILES['surat_lahir']['name'];
    $surat_lahir_tmp = $_FILES['surat_lahir']['tmp_name'];
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];
    $ktp_ayah_name = $_FILES['ktp_ayah']['name'];
    $ktp_ayah_tmp = $_FILES['ktp_ayah']['tmp_name'];
    $ktp_ibu_name = $_FILES['ktp_ibu']['name'];
    $ktp_ibu_tmp = $_FILES['ktp_ibu']['tmp_name'];
    $buku_nikah_name = $_FILES['buku_nikah']['name'];
    $buku_nikah_tmp = $_FILES['buku_nikah']['tmp_name'];
    $lainnya_name = $_FILES['lainnya']['name'];
    $lainnya_tmp = $_FILES['lainnya']['tmp_name'];

    $surat_pengantar = $tgl_pengajuan . "_" . "Surat Pengantar" . "_" . $nama_anak . "_" . $surat_pengantar_name;
    $surat_lahir = $tgl_pengajuan . "_" . "Surat Lahir" . "_" . $nama_anak . "_" . $surat_lahir_name;
    $kk = $tgl_pengajuan . "_" . "KK" . "_" . $nama_anak . "_" . $kk_name;
    $ktp_ayah = $tgl_pengajuan . "_" . "KTP Ayah" . "_" . $nama_anak . "_" . $ktp_ayah_name;
    $ktp_ibu = $tgl_pengajuan . "_" . "KTP Ibu" . "_" . $nama_anak . "_" . $ktp_ibu_name;
    $buku_nikah = $tgl_pengajuan . "_" . "Buku Nikah" . "_" . $nama_anak . "_" . $buku_nikah_name;
    $lainnya = $tgl_pengajuan . "_" . "Lainnya" . "_" . $nama_anak . "_" . $lainnya_name;
    move_uploaded_file($surat_pengantar_tmp, "../file desa/akte lahir/" . $surat_pengantar);
    move_uploaded_file($surat_lahir_tmp, "../file desa/akte lahir/" . $surat_lahir);
    move_uploaded_file($kk_tmp, "../file desa/akte lahir/" . $kk);
    move_uploaded_file($ktp_ayah_tmp, "../file desa/akte lahir/" . $ktp_ayah);
    move_uploaded_file($ktp_ibu_tmp, "../file desa/akte lahir/" . $ktp_ibu);
    move_uploaded_file($buku_nikah_tmp, "../file desa/akte lahir/" . $buku_nikah);
    move_uploaded_file($lainnya_tmp, "../file desa/akte lahir/" . $lainnya);

    $simpanakte = "INSERT INTO akte_baru_lahir(noreg, no_hp, no_kk, nama_anak, jk_anak, tgl_lahir, jenis_kelahiran, anak_ke, berat, panjang, nik_ibu, nama_ibu, nik_ayah, nama_ayah, surat_pengantar, surat_lahir, kk, ktp_ayah, ktp_ibu, buku_nikah, tgl_pengajuan, petugas, status, ket, detail, lainnya) VALUES ('$noregis', '$no_hp', '$no_kk', '$nama_anak', '$jk_anak', '$tgl_lahir', '$jenis_kelahiran', '$anak_ke', '$berat', '$panjang', '$nik_ibu', '$nama_ibu', '$nik_ayah', '$nama_ayah', '$surat_pengantar', '$surat_lahir', '$kk', '$ktp_ayah', '$ktp_ibu', '$buku_nikah', '$tgl_pengajuan', '$petugas', '$status', '$ket', '$detail', '$lainnya')";
    if ($connection->query($simpanakte)) {
        header("location:../akte_lahir.php");
    } else {
        header("location:../akte_lahir.php");
    }
} else if ($_GET['act'] == 'deleteakte') {
    $id = $_GET['id'];
    $get_file = "SELECT surat_pengantar, surat_lahir, kk, ktp_ayah, ktp_ibu, buku_nikah, lainnya FROM akte_baru_lahir WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/akte lahir/" . $file_old['surat_pengantar']);
    unlink("../file desa/akte lahir/" . $file_old['surat_lahir']);
    unlink("../file desa/akte lahir/" . $file_old['kk']);
    unlink("../file desa/akte lahir/" . $file_old['ktp_ayah']);
    unlink("../file desa/akte lahir/" . $file_old['ktp_ibu']);
    unlink("../file desa/akte lahir/" . $file_old['buku_nikah']);
    unlink("../file desa/akte lahir/" . $file_old['lainnya']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM akte_baru_lahir WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../akte_lahir.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'aktemati') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM akte_mati WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 24, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/am";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $no_hp = $_POST['no_hp'];
    $nik_pelapor = $_POST['nik_pelapor'];
    $nama_pelapor = $_POST['nama_pelapor'];
    $nik_mati = $_POST['nik_mati'];
    $nama_mati = $_POST['nama_mati'];
    $petugas = $_POST['petugas'];
    $status = "pending";
    $ket = "";

    //files
    $ktp_pelapor_name = $_FILES['ktp_pelapor']['name'];
    $ktp_pelapor_tmp = $_FILES['ktp_pelapor']['tmp_name'];
    $ktp_mati_name = $_FILES['ktp_mati']['name'];
    $ktp_mati_tmp = $_FILES['ktp_mati']['tmp_name'];
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];
    $surat_kuning_name = $_FILES['surat_kuning']['name'];
    $surat_kuning_tmp = $_FILES['surat_kuning']['tmp_name'];
    $surat_mati_desa_name = $_FILES['surat_mati_desa']['name'];
    $surat_mati_desa_tmp = $_FILES['surat_mati_desa']['tmp_name'];
    $formulir_mati_name = $_FILES['formulir_mati']['name'];
    $formulir_mati_tmp = $_FILES['formulir_mati']['tmp_name'];

    $ktp_pelapor = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KTP Pelapor" . "_" . $ktp_pelapor_name;
    $ktp_mati = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KTP Mati" . "_" . $ktp_mati_name;
    $kk = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KK" . "_" . $kk_name;
    $surat_kuning = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "Surat Kuning" . "_" . $surat_kuning_name;
    $surat_mati_desa = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "Surat Mati Desa" . "_" . $surat_mati_desa_name;
    $formulir_mati = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "SPTJM" . "_" . $formulir_mati_name;
    move_uploaded_file($ktp_pelapor_tmp, "../file desa/akte mati/" . $ktp_pelapor);
    move_uploaded_file($ktp_mati_tmp, "../file desa/akte mati/" . $ktp_mati);
    move_uploaded_file($kk_tmp, "../file desa/akte mati/" . $kk);
    move_uploaded_file($surat_kuning_tmp, "../file desa/akte mati/" . $surat_kuning);
    move_uploaded_file($surat_mati_desa_tmp, "../file desa/akte mati/" . $surat_mati_desa);
    move_uploaded_file($formulir_mati_tmp, "../file desa/akte mati/" . $formulir_mati);

    $simpanaktemati = "INSERT INTO akte_mati(noreg, no_hp, nik_pelapor, nama_pelapor, nik_mati, nama_mati, ktp_pelapor, ktp_mati, kk, surat_kuning, surat_mati_desa, formulir_mati, tgl_pengajuan, petugas, status, ket) VALUES ('$noregis', '$no_hp', '$nik_pelapor', '$nama_pelapor', '$nik_mati', '$nama_mati', '$ktp_pelapor', '$ktp_mati', '$kk', '$surat_kuning', '$surat_mati_desa', '$formulir_mati', '$tgl_hari_ini', '$petugas', '$status' , '$ket')";
    if ($connection->query($simpanaktemati)) {
        header("location:../akte_kematian.php");
    } else {
        header("location:../akte_kematian.php");
    }
} else if ($_GET['act'] == 'deletemati') {
    $id = $_GET['id'];
    $get_file = "SELECT ktp_pelapor, ktp_mati, kk, surat_kuning, surat_mati_desa FROM akte_mati WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/akte mati/" . $file_old['ktp_pelapor']);
    unlink("../file desa/akte mati/" . $file_old['ktp_mati']);
    unlink("../file desa/akte mati/" . $file_old['kk']);
    unlink("../file desa/akte mati/" . $file_old['surat_kuning']);
    unlink("../file desa/akte mati/" . $file_old['surat_mati_desa']);
    unlink("../file desa/akte mati/" . $file_old['formulir_mati']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM akte_mati WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../akte_kematian.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'suratpindah') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM surat_pindah WHERE tgl_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 24, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/sp";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $nik_pelapor = $_POST['nik_pelapor'];
    $nama_pelapor = $_POST['nama_pelapor'];
    $no_hp = $_POST['no_hp'];
    $alamat_old = $_POST['alamat_old'];
    $alamat_new = $_POST['alamat_new'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";

    //files
    $ktp_name = $_FILES['ktp']['name'];
    $ktp_tmp = $_FILES['ktp']['tmp_name'];
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];
    $surat_desa_name = $_FILES['surat_desa']['name'];
    $surat_desa_tmp = $_FILES['surat_desa']['tmp_name'];

    $ktp = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KTP Pelapor" . "_" . $ktp_name;
    $kk = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KK Pelapor" . "_" . $kk_name;
    $surat_desa = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "Surat Desa" . "_" . $surat_desa_name;
    move_uploaded_file($ktp_tmp, "../file desa/surat pindah/" . $ktp);
    move_uploaded_file($kk_tmp, "../file desa/surat pindah/" . $kk);
    move_uploaded_file($surat_desa_tmp, "../file desa/surat pindah/" . $surat_desa);

    $simpansurat = "INSERT INTO surat_pindah(noreg, nik_pelapor, nama_pelapor, no_hp, alamat_old, alamat_new, tgl_pengajuan, ktp, kk, surat_desa, status, petugas, ket) VALUES ('$noregis', '$nik_pelapor', '$nama_pelapor', '$no_hp', '$alamat_old', '$alamat_new', '$tgl_hari_ini', '$ktp', '$kk', '$surat_desa', '$status', '$petugas', '$ket')";

    if ($connection->query($simpansurat)) {
        header("location:../surat_pindah.php");
    } else {
        header("location:../surat_pindah.php");
    }
} else if ($_GET['act'] == 'ktp') {
    $tgl_hari_ini = date("Y-m-d");
    $q = mysqli_query($connection, "SELECT max(noreg) AS noTerbesar FROM ktp WHERE tanggal_pengajuan='$tgl_hari_ini'");
    $data = mysqli_fetch_array($q);
    $noregis = $data['noTerbesar'];
    $urutan = (int) substr($noregis, 25, 3);
    $urutan++;
    $kode = "1219";
    $huruf = "didesa/ktp";
    $tgl = date("dmY");
    $noregis = $kode . "/" . $huruf . "/" . $tgl . "/" . sprintf("%03s", $urutan);

    $nik_pelapor = $_POST['nik'];
    $nama_pelapor = $_POST['nama'];
    $no_hp = $_POST['no_hp'];
    $alamat = $_POST['alamat'];
    $status = "pending";
    $petugas = $_POST['petugas'];
    $ket = "";

    //files
    $kk_name = $_FILES['kk']['name'];
    $kk_tmp = $_FILES['kk']['tmp_name'];

    $kk = $tgl_hari_ini . "_" . $nama_pelapor . "_" . "KK Pelapor" . "_" . $kk_name;
    move_uploaded_file($kk_tmp, "../file desa/ktp/" . $kk);

    $simpansurat = "INSERT INTO ktp(noreg, nik, nama, no_hp, alamat, tanggal_pengajuan, kk, petugas, status, ket) VALUES ('$noregis', '$nik_pelapor', '$nama_pelapor', '$no_hp', '$alamat', '$tgl_hari_ini', '$kk', '$petugas', '$status', '$ket')";

    if ($connection->query($simpansurat)) {
        header("location:../ktp_baru.php");
    } else {
        header("location:../ktp_baru.php");
    }
} else if ($_GET['act'] == 'deletektp') {
    $id = $_GET['id'];
    $get_file = "SELECT kk FROM ktp WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/ktp/" . $file_old['kk']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM ktp WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../ktp_baru.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
} else if ($_GET['act'] == 'deletesurat') {
    $id = $_GET['id'];
    $get_file = "SELECT ktp, kk, surat_desa FROM surat_pindah WHERE id='$id'";
    $data_file = mysqli_query($connection, $get_file);
    $file_old = mysqli_fetch_assoc($data_file);
    unlink("../file desa/surat pindah/" . $file_old['ktp']);
    unlink("../file desa/surat pindah/" . $file_old['kk']);
    unlink("../file desa/surat pindah/" . $file_old['surat_pindah']);

    //query hapus
    $querydelete = mysqli_query($connection, "DELETE FROM surat_pindah WHERE id = '$id'");

    if ($querydelete) {
        # redirect ke index.php
        header("location:../surat_pindah.php");
    } else {
        echo "ERROR, data gagal dihapus" . mysqli_error($connection);
    }

    mysqli_close($connection);
}
